import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import pickle
import pdb

import pandas as pd
import plotly.graph_objs as go
import plotly.express as px
import matplotlib.pyplot as plt

pct_df = pickle.load(open('percent_positive.pickle', 'rb'))
pct_df_first = pct_df.filter(['Basic', 'Oscars', 'Martingale', 'Counter'], axis=1)
pct_df_martingale = pct_df.filter(['Martingale', 'Martingale Limit'], axis=1)
pct_df_oscars = pct_df.filter(['Basic', 'Oscars', 'Oscars 3-Streak', 'Oscars 4-Streak', 'Oscars 5-Streak'], axis=1)


max_bet_df = pickle.load(open('max_bet_required.pickle', 'rb'))
max_bet_df_martingale = max_bet_df.filter(['Martingale'], axis=1)
max_bet_df_martingale_limit = max_bet_df.filter(['Martingale Limit'], axis=1)
max_bet_df_counter = max_bet_df.filter(['Counter'], axis=1)

#pdb.set_trace()


#external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
#external_stylesheets = ['css_style.css']

#app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app = dash.Dash(__name__)
app.config.suppress_callback_exceptions = True

app.layout = html.Div(className = "container",children = [
    html.Div(className = "grey-group", children=[
        html.Div(className = "row",children=[
            html.Div(className="one-half column", children = [
                html.H1('BlackJack Strategy dashboard')
            ]),
            html.Div(className="one-half column", children = [
                html.H4('Hand selection slider'),
                html.P('Drag the slider to see how the simulations change over the 1000 hands'),
                dcc.Input(
                    id='hand_input',
                    placeholder='Choose a hand number between 0 & 999...',
                    type='number',
                    value=1,
                    min=1,
                    max=1000
                ),
                dcc.Slider(
                    id='hand-slider',
                    min=1,
                    max=len(pct_df_first.index),
                    value=1,
                    marks={str(num-1): str(num-1) for num in range(1, len(pct_df_first.index), 100)} ,
                    step=1,
                    updatemode='drag'
                )
                
            ], style={'margin': 30})
        ])
    ]),

    html.Div(className = "grey-group", children=[

        html.Div(className = "row", children=[
            html.Div(className ="twelve columns", children = [
                html.H3(children=['Martingale appears to be the dominant strategy'], style={'text-align': 'center'}),
                html.P(children=['Each strategy consists of 100 players with 1000 hands per player per strategy. The winning percentages for the 4 strategies are given below: basic, martingale, oscars, and card counting'])
            ])
        ]),


        html.Div(className="row", children=[
            html.Div(className="six columns", children = [
                dcc.Graph(  id='percent_bar_chart',
                            figure={ 
                                'data': [go.Bar( x = pct_df_first.loc[0].sort_values(ascending=True).values.tolist(),
                                                 y = pct_df_first.loc[0].sort_values(ascending=True).index.tolist(),
                                                orientation='h',
                                                marker = {'color': pct_df_first.loc[0].sort_values(ascending=True).values.tolist(), 'colorscale': px.colors.diverging.RdYlGn})],
                                'layout': {
                                    'xaxis':{
                                        'autorange': False,
                                        'range': [0,1,0.1]
                                    },


                                }
                            }
                ),
                html.P('Percentage of the 100 players with positive winnings after X-hands per strategy')
            ]),

            html.Div(className="six columns", children = [
                dcc.Graph(  id='percent_line_chart',
                            figure={
                                'data': [go.Scatter(x = [i for i in range(0,len(pct_df_first.index))],
                                                    y = pct_df[col],
                                                    name = col) for col in pct_df_first.columns.values.tolist()],
                                'layout':{
                                    'shapes':[{
                                            'type': 'line',
                                            'x0': 0,
                                            'y0': 0,
                                            'x1': 0,
                                            'y1': 1,
                                            'line': {
                                                'color': 'rgb(0, 0, 0)',
                                                'width': 3,
                                                'dash': 'dashdot'
                                            }
                                        }],
                                    "plot_bgcolor":"#1E1E1E",
                                    "paper_bgcolor":"#1E1E1E"

                                }
                            }
                ),
                html.P('Running view of the percentage of winners per strategy')
            ])
        ])
    ]),


    html.Div(className = "grey-group", children=[
        html.Div(className = "row", children=[
            html.Div(className="twelve columns", children = [
                html.H3('Martingale has two distinct limitations that make it impossible')
            ])
        ]),

        html.Div(className="row", children=[
            html.Div(className="six columns", children=[
                html.P('Extremely high stakes to maintain strategy - wide variance'),
                dcc.Graph(  id = 'bet_distribution_martingale',
                            figure={'data': [go.Violin( x = [col]*100,
                                    y = max_bet_df[col],
                                    name = col) for col in max_bet_df_martingale.columns.values.tolist()],
                                    'layout':{
                                        "plot_bgcolor":"#1E1E1E",
                                        "paper_bgcolor":"#1E1E1E",
                                        'font':{
                                            'color': '#FFFFFF'
                                        }
                                    }
                            }
                ),
                html.P('On average over 100 players, Martingale will require a bet of 1024 times your base bet to maintain the strategy. $5 = $5120 bet to break a likely losing streak over 1000 hands. The distribution is heavily skewed, so you might get even worse luck than that! Get ready to pay up!')
            ]),

            html.Div(className="six columns", children=[
                html.P('Table limits prevent this from winning in long term'),
                dcc.Graph(  id='percent_line_chart_martingale',
                            figure={
                                'data': [go.Scatter(x = [i for i in range(0,len(pct_df_martingale.index))],
                                                    y = pct_df[col],
                                                    name = col) for col in pct_df_martingale.columns.values.tolist()],
                                'layout':{
                                    'shapes':[{
                                            'type': 'line',
                                            'x0': 0,
                                            'y0': 0,
                                            'x1': 0,
                                            'y1': 1,
                                            'line': {
                                                'color': 'rgb(255, 255, 255)',
                                                'width': 3,
                                                'dash': 'dashdot'
                                            }
                                        }],
                                    'font':{
                                        'color': '#FFFFFF'
                                    }
                                }
                            }
                ),
                html.P('Casinos prevent MASSIVE bets on their tables via table limits. Including limits in the simulation cripples Martingale - at some point you can\'t keep doubling your bet to recoup losses.')
            ])

        ])
    ]),

    html.Div(className = "grey-group", children=[
        html.Div(className="row", children=[
            html.Div(className='twelve columns', children=[
                html.H3('Oscars can be \"smarter\" if we limit the win streaks'),
                html.P('Unlike pure Oscars, we revert back to our base bet after either 3, 4, or 5 wins in a row. A never ending win streak is clearly false, but perhaps there is an advantage to allowing a short spurt of winning hands.')
            ])
        ]),

        html.Div(className="row", children=[

            html.Div(className="six columns", children = [
                dcc.Graph(  id='percent_bar_chart_oscars',
                            figure={ 
                                'data': [go.Bar( x = pct_df_oscars.loc[0].sort_values(ascending=True).values.tolist(),
                                                 y = pct_df_oscars.loc[0].sort_values(ascending=True).index.tolist(),
                                                orientation='h',
                                                marker = {'color': pct_df_oscars.loc[0].sort_values(ascending=True).values.tolist(), 'colorscale': px.colors.diverging.RdYlGn})],
                                'layout': {
                                    'xaxis':{
                                        'autorange': False,
                                        'range': [0,1,0.1]
                                    },
                                    'font':{
                                        'color': '#FFFFFF'
                                    }
                                }
                            }
                ),

                html.P('Introducing limits on win streaks helps Oscars, but they don\'t give an advantage over basic betting. Relying on win streaks is a myth.')
            ], style={'margin-left': 40}),

            html.Div(className="six columns", children = [
                dcc.Graph(  id='percent_line_chart_oscars',
                            figure={
                                'data': [go.Scatter(x = [i for i in range(0,len(pct_df_oscars.index))],
                                                    y = pct_df[col],
                                                    name = col) for col in pct_df_oscars.columns.values.tolist()],
                                'layout':{
                                    'shapes':[{
                                            'type': 'line',
                                            'x0': 0,
                                            'y0': 0,
                                            'x1': 0,
                                            'y1': 1,
                                            'line': {
                                                'color': 'rgb(255, 255, 255)',
                                                'width': 3,
                                                'dash': 'dashdot'
                                            }
                                        }],
                                    'font':{
                                        'color': '#FFFFFF'
                                    }
                                }
                            }
                ),

                html.P('Over 1000 hands, Oscars on a 4-win streak limit is better than either 3 or 5 streaks. This may make the game way more exciting than basic betting, but also your bank roll won\'t last as long. Value your entertainment dollar before committing to this strategy')
            ])

        ])
    ])
 
])

#callbacks for input control updates
@app.callback(
    Output('hand_input', 'value'),
    [Input('hand-slider', 'value')])
def update_input(selected_hand):
    return selected_hand

@app.callback(
    Output('hand-slider', 'value'),
    [Input('hand_input', 'n_submit')], 
    [State('hand_input', 'value')])
def update_slider(n_submit, input_hand):
    return input_hand


#callbacks for RAG bar charts
@app.callback(
    Output('percent_bar_chart', 'figure'),
    [Input('hand-slider', 'value')])
def update_bars(selected_hand):
    return {'data': [go.Bar( x = pct_df_first.loc[selected_hand - 1].sort_values(ascending=True).values.tolist(),
                            y = pct_df_first.loc[selected_hand - 1].sort_values(ascending=True).index.tolist(),
                            orientation='h',
                            marker = {'color': pct_df_first.loc[selected_hand - 1].sort_values(ascending=True).values.tolist(), 'colorscale': px.colors.diverging.RdYlGn})],
            'layout': {
                'xaxis':{
                    'autorange': False,
                    'range': [0,1,0.1]
                },

                "plot_bgcolor":"#1E1E1E",
                "paper_bgcolor":"#1E1E1E",
                'font':{
                    'color': '#FFFFFF'
                }
            }
    }

@app.callback(
    Output('percent_bar_chart_oscars', 'figure'),
    [Input('hand-slider', 'value')])
def update_bars(selected_hand):
    return {'data': [go.Bar( x = pct_df_oscars.loc[selected_hand - 1].sort_values(ascending=True).values.tolist(),
                            y = pct_df_oscars.loc[selected_hand - 1].sort_values(ascending=True).index.tolist(),
                            orientation='h',
                            marker = {'color': pct_df_oscars.loc[selected_hand - 1].sort_values(ascending=True).values.tolist(), 'colorscale': px.colors.diverging.RdYlGn})],
            'layout': {
                'xaxis':{
                    'autorange': False,
                    'range': [0,1,0.1]
                },
                "plot_bgcolor":"#1E1E1E",
                "paper_bgcolor":"#1E1E1E",
                'font':{
                    'color': '#FFFFFF'
                }
            }

            
    }

#callbacks for reference lines on line charts

@app.callback(
    Output('percent_line_chart', 'figure'),
    [Input('hand-slider', 'value')])
def update_lines(selected_hand):
    return {'data': [go.Scatter(x = [i for i in range(0,len(pct_df_first.index))],
                                y = pct_df[col],
                                name = col) for col in pct_df_first.columns.values.tolist()],
            'layout':{
                'shapes':[{
                        'type': 'line',
                        'x0': selected_hand,
                        'y0': 0,
                        'x1': selected_hand,
                        'y1': 1,
                        'line': {
                            'color': 'rgb(255, 255, 255)',
                            'width': 3,
                            'dash': 'dashdot'
                        }
                    }],
                "plot_bgcolor":"#1E1E1E",
                "paper_bgcolor":"#1E1E1E",
                'font':{
                    'color': '#FFFFFF'
                }
            }
    }


@app.callback(
    Output('percent_line_chart_martingale', 'figure'),
    [Input('hand-slider', 'value')])
def update_lines(selected_hand):
    return {'data': [go.Scatter(x = [i for i in range(0,len(pct_df_martingale.index))],
                                y = pct_df[col],
                                name = col) for col in pct_df_martingale.columns.values.tolist()],
            'layout':{
                'shapes':[{
                        'type': 'line',
                        'x0': selected_hand,
                        'y0': 0,
                        'x1': selected_hand,
                        'y1': 1,
                        'line': {
                            'color': 'rgb(255, 255, 255)',
                            'width': 3,
                            'dash': 'dashdot'
                        }
                    }],
                "plot_bgcolor":"#1E1E1E",
                "paper_bgcolor":"#1E1E1E",
                'font':{
                    'color': '#FFFFFF'
                }
            }
    }

@app.callback(
    Output('percent_line_chart_oscars', 'figure'),
    [Input('hand-slider', 'value')])
def update_lines(selected_hand):
    return {'data': [go.Scatter(x = [i for i in range(0,len(pct_df_oscars.index))],
                                y = pct_df[col],
                                name = col) for col in pct_df_oscars.columns.values.tolist()],
            'layout':{
                'shapes':[{
                        'type': 'line',
                        'x0': selected_hand,
                        'y0': 0,
                        'x1': selected_hand,
                        'y1': 1,
                        'line': {
                            'color': 'rgb(255, 255, 255)',
                            'width': 3,
                            'dash': 'dashdot'
                        }
                    }],
                "plot_bgcolor":"#1E1E1E",
                "paper_bgcolor":"#1E1E1E",
                'font':{
                    'color': '#FFFFFF'
                }
            }
    }

if __name__ == '__main__':
    app.run_server(debug=True)