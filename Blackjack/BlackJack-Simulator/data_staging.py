import scipy
import pandas as pd
import pickle
import pdb
import math

def pickleWrite(df):
	filename = df.name + ".pickle"
	with open(filename, 'wb') as handle:
		pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
	#read in all pickle files to dictionary
	basic = pickle.load(open('data_Basic.pickle', 'rb'))
	oscars = pickle.load(open('data_Oscars.pickle', 'rb'))
	oscars_limit = pickle.load(open('data_Oscars_limit.pickle', 'rb'))
	oscars_3streak = pickle.load(open('data_Oscars_3streak.pickle','rb'))
	oscars_4streak = pickle.load(open('data_Oscars_4streak.pickle','rb'))
	oscars_5streak = pickle.load(open('data_Oscars_5streak.pickle','rb'))
	martingale = pickle.load(open('data_Martingale.pickle', 'rb'))
	martingale_limit = pickle.load(open('data_Martingale_limit.pickle', 'rb'))
	counter = pickle.load(open('data_Counter.pickle', 'rb'))
	#pdb.set_trace()
	#define data structures for plotting
	#percentage of hands above zero
	pct_df = pd.DataFrame(columns=['Basic', 'Oscars', 'Oscars Limit', 'Oscars 3-Streak', 'Oscars 4-Streak', 'Oscars 5-Streak', 'Martingale', 'Martingale Limit', 'Counter'])
	pct_df.name = "percent_positive"
	max_bet_df = pd.DataFrame(columns=['Basic', 'Oscars', 'Oscars Limit', 'Martingale', 'Martingale Limit', 'Counter'])
	max_bet_df.name = "max_bet_required"

	for i in range(basic[0].shape[0]):
		#win hand percentage
		basic_pct = 0.0
		oscars_pct = 0.0
		oscars_limit_pct = 0.0
		oscars_3streak_pct = 0.0
		oscars_4streak_pct = 0.0
		oscars_5streak_pct = 0.0
		martingale_pct = 0.0
		martingale_limit_pct = 0.0
		counter_pct = 0.0
		
		for j in range(len(basic)):
			if basic[j].iloc[i]['money'] >= 0:
				basic_pct+=1.0
			if oscars[j].iloc[i]['money'] >= 0:
				oscars_pct+=1.0
			if oscars_limit[j].iloc[i]['money'] >= 0:
				oscars_limit_pct+=1.0	
			if oscars_3streak[j].iloc[i]['money'] >= 0:
				oscars_3streak_pct+=1.0
			if oscars_4streak[j].iloc[i]['money'] >= 0:
				oscars_4streak_pct+=1.0
			if oscars_5streak[j].iloc[i]['money'] >= 0:
				oscars_5streak_pct+=1.0
			if martingale[j].iloc[i]['money'] >= 0:
				martingale_pct+=1.0
			if martingale_limit[j].iloc[i]['money'] >= 0:
				martingale_limit_pct+=1.0
			if counter[j].iloc[i]['money'] >= 0:
				counter_pct+=1.0
		basic_pct = basic_pct / float(len(basic))
		oscars_pct = oscars_pct / float(len(basic))
		oscars_limit_pct = oscars_limit_pct / float(len(basic))
		oscars_3streak_pct = oscars_3streak_pct / float(len(basic))
		oscars_4streak_pct = oscars_4streak_pct / float(len(basic))
		oscars_5streak_pct = oscars_5streak_pct / float(len(basic))
		martingale_pct = martingale_pct / float(len(basic))
		martingale_limit_pct = martingale_limit_pct / float(len(basic))
		counter_pct = counter_pct / float(len(basic))
		pct_df.loc[i] = [basic_pct, oscars_pct, oscars_limit_pct, oscars_3streak_pct, oscars_4streak_pct, oscars_5streak_pct, martingale_pct, martingale_limit_pct, counter_pct]
	pickleWrite(pct_df)

	for i in range(len(basic)):
		#figure out max bet required for that performance
		basic_bet_req = 1
		oscars_bet_req = 1
		oscars_limit_bet_req = 1
		martingale_bet_req = martingale[i]["stake"].max()
		martingale_limit_bet_req = martingale_limit[i]["stake"].max()
		counter_bet_req = counter[i]["stake"].max()
		max_bet_df.loc[i] = [basic_bet_req, oscars_bet_req, oscars_limit_bet_req, martingale_bet_req, martingale_limit_bet_req, counter_bet_req]
	pickleWrite(max_bet_df)

	pdb.set_trace()



	#populate data structures

	#plot to dash
