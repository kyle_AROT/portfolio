# Blackjack

For Labor Day this year, I traveled to Las Vegas with one of my close friends for her birthday. Her favorite game to play in the casino is Blackjack, and we planned to sit for a few hours playing together in her favorite casino, the Cosmopolitan.

I didn't have much experience gambling prior to this trip, so I researched betting strategies for Blackjack that most people commonly use. Some of these strategies claim to "beat the house"; however, my understanding of statistics knows this is (legally) impossible to do. So, I formed a data analysis and podcast episode around the question if some betting strategies are better than others.

## Challenge

Structured, existing data were hard to find. A number of academic studies exist explaining the theory/probabilities behind certain strategies; however, the raw data did not accompany these articles. Instead, I found a [simulation on Github](https://github.com/seblau/BlackJack-Simulator/blob/master/BlackJack.py) to which I could add my own framework for analysis.

## Data Sources and Technology

* Python - simulation, Pandas
* Dash by Plotly - visualization
* AWS - cloud hosting

## Analysis and Results

The first change I made to the starting code involved adding different types of players based on betting strategy: basic, card counting, Martingale, Oscars. Next, I wrapped each game instance in a simulation of 100 players, 1000 hands per player to generate enough data for analysis. At each stage of the simulation, I saved the data into Pickle files for easy loading in other Python scripts.

**Strategies**

* Basic - bet per hand is unchanged, always the minimum amount. This is considered the control strategy
* Martingale - bet doubles for each loss; reverts to minimum on each win (Mentality - losing streaks aren't forever)
* Oscars - bet doubles for each win; reverts to minimum on each loss (Mentality - everyone is due a "hot streak")
* Card Counting - bet stays low until the dealers "count" is favorable; once favorable, bet jumps up like a step function

**Takeaways**

After visualizing the data, one strategy appeared dominant: the Martingale strategy. After 1000 hands for 100 players, almost all of these players were profitable. At first, I thought I made errors in simulating this strategy, but with a bit more research I realized that the data were correct. Casinos also know the power of the Martingale strategy, and casinos have a concept called "table limits" to counteract players that try this.

Table limits set a minimum and maximum amount that a player can bet per hand. On the surface, this seems in favor of the player so that a player doesn't feel tempted to blow $100K on a single hand (i.e. gamble responsibly). However, the table limit exists for the casinos benefit because after a certain number of iterations, the max table limit stops the Martingale strategy from working. The Martingale strategy requires doubling on each loss to recoup previous losses. Once a table limit is reached, players can't double again, and those losses are realized and difficult to recover. After implementing a table limit in the simulation, the Martingale strategy definitely appeared less dominant.

Secondly, the Oscars strategy was the worst performing because it relied on never losing, which is unrealistic. However, limiting Oscars (unlink limiting Martingale) can have advantages. Resetting to a minimum bet after 4 consecutive wins with Oscars is the best performant version of this strategy. However, no version of Oscars is better than a basic strategy, meaning that the house advantage on the game is real. No winning streaks are due to any player at any time.

The only strategy that could sustain a somewhat consistent profit is card counting. While card counting is not illegal, it is frowned upon by casinos, and staff are legally allowed to ask a card counter to leave a table. Card counting is the only strategy to alter the house edge because it is the only strategy that keeps track of the changing probabilities in the decks of cards. As certain cards get dealt, the probability of other cards appearing next either increase or decrease, slightly altering the house edge. Card counting allows bigger bets with the house edge shifts to the players favor after other cards get dealt. Disclaimer: I am not advocating for card counting

The full analysis of this project can be found here:

[Plotly Dashboard - Another Round of Thoughts](http://blackjack-dev.us-west-2.elasticbeanstalk.com/)

**Audio**

As mentioned, I produced 2 full podcast episodes for this analysis. If you're interested in listening, the Spotify link is below:

[Another Round of Thoughts](https://open.spotify.com/show/15AVd5DPk6n0aId8hfrbwv)