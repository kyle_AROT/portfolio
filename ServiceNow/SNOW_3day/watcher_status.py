


def CL_watcherStatus(ticket):
	watch_ok = 'Fine'
	if(not ticket['Watch List']):
		if(ticket['Opened By'] != 'Jonathan Toynton'):
			watch_ok = 'Add Jonathan'
	elif(ticket['Opened By'] != 'Jonathan Toynton' and 'Jonathan Toynton' not in ticket['Watch List']):
		watch_ok = 'Add Jonathan'
	return watch_ok

def RD_watcherStatus(ticket):
	watch_ok = 'Fine'
	if(not ticket['Watch List']):
		if(ticket['Opened By'] != 'Ben Conrad'):
			watch_ok = 'Add Ben'
	elif(ticket['Opened By'] != 'Ben Conrad' and 'Ben Conrad' not in ticket['Watch List']):
		watch_ok = 'Add Ben'
	return watch_ok


def IN_watcherStatus(ticket):
	watch_ok = 'Fine'
	if(not ticket['Watch List']):
		if(ticket['Opened By'] == 'Mark Glenewinkel'):
			watch_ok = 'Add Kyle'
		elif(ticket['Opened By'] == 'Kyle Unruh'):
			watch_ok = 'Add Mark'
		else:
			watch_ok = 'Add Mark and Kyle'
	elif(ticket['Opened By'] == 'Mark Glenewinkel'):
		if ("Kyle Unruh" not in ticket['Watch List']):
			watch_ok = 'Add Kyle'
	elif(ticket['Opened By'] == 'Kyle Unruh'):
		if ("Mark Glenewinkel" not in ticket['Watch List']):
			watch_ok = 'Add Mark'
	elif("Kyle Unruh" not in ticket['Watch List'] or "Mark Glenewinkel" not in ticket['Watch List']):
		if("Kyle Unruh" in ticket['Watch List']):
			watch_ok = 'Add Mark'
		elif("Mark Glenewinkel" in ticket['Watch List']):
			watch_ok = 'Add Kyle'
		else:
			watch_ok = 'Add Mark and Kyle'
	return watch_ok


def calcWatcherStatus(division, ticket):
	watcherStatus = ''
	if division == 'IN':
		watcherStatus = IN_watcherStatus(ticket)
	elif division == 'RD':
		watcherStatus = RD_watcherStatus(ticket)
	elif division == 'CL':
		watcherStatus = CL_watcherStatus(ticket)
	else:
		watcherStatus = 'Undefined for division: get watcher requirements'
	return watcherStatus