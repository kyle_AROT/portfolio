import sys, csv
import sendemail
import csv, sys, datetime, os
from sqlalchemy import *
import pandas as pd
import pyhdb, pymysql
import pdb
from datetime import datetime, date
import sendemail as sendEmail
import snow_3day_html as html
import watcher_status as watchers


def readFromDB(div):
	#get divisions in scope
	dept_sql_query = '''
		SELECT DISTINCT DEPARTMENT
			FROM "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/PEOPLE"
			WHERE DIVISION = '<DIVISION>'
	'''
	db_hana = create_engine('hana+pyhdb://kylunr01:InfrastructureOps2019!@fusionpro.arm.com:32515', echo=False)
	dept_result = pd.read_sql(dept_sql_query.replace('<DIVISION>', div),db_hana)
	dept_string = ''
	for dept in dept_result.iterrows():
		dept_string = dept_string + '\'' + dept[1]['department'] + '\','
	dept_string = dept_string[:-1]

	#get tickets with those divisions
	ticket_sql = '''
	SELECT 	`number` as 'Ticket Number'
		,	sys_id as 'Ticket System Id'
		,	active as 'Active'
		,	Department_Name as 'Department'
		,	u_esm_function as 'Ticket Function'
		,	'Task' as 'Ticket Type'
		,	short_description as 'Ticket Description'
		,	u_ipg_company as 'External Company'
		,	opened_at as 'Opened Date'
		,	dv_opened_by as 'Opened By'
		,	CASE WHEN dv_assigned_to is NULL THEN 'NONE' ELSE dv_assigned_to END as 'Assigned To'
		,	CASE WHEN dv_assignment_group IS NULL THEN 'NONE' ELSE dv_assignment_group END AS 'Assignment Group'
		,	approval as 'Approval'
		,	u_state as 'Ticket Status'
		,	u_stage as 'Ticket Stage'
		,	sys_updated_on as 'Last Update Date'
		,	sys_updated_by as 'Last Update By'
		,	closed_at as 'Closed Date'
		,	dv_closed_by as 'Closed By'
		,	'sc_req_item' as 'URL Helper'
		,	dv_watch_list as 'Watch List'
		FROM mirror.ip_group_legal_procurement_tasks
		where Department_Name in (<DEPT>)
			and active = 1
			and u_state not like '%%Close%%' and u_state not in ('Fulfilled', 'Resolved')
			and u_esm_function <> 'People_Team'
			and u_stage not like '%%Complete%%' and u_stage not like '%%Fulfil%%' and u_stage not like '%%Reject%%' and u_stage not like '%%Delivery%%'
		
		union

		select	`number` as 'Ticket Number'
			,	sys_id as 'Ticket System Id'
			,	case when resolved_at is null then 1 else 0 end as 'Active'
			,	Department_Name as 'Department'
			,	'IT Incident' as 'Ticket Function'
			,	'Incident' as 'Ticket Type'
			,	short_description as 'Ticket Description'
			,	dv_u_vendor as 'External Company'
			,	mirror_created_on as 'Opened Date'
			,	dv_caller_id as 'Opened By'
			,	CASE WHEN dv_assigned_to is NULL THEN 'NONE' ELSE dv_assigned_to END as 'Assigned To'
			,	CASE WHEN dv_assignment_group IS NULL THEN 'NONE' ELSE dv_assignment_group END AS 'Assignment Group'
			,	null as 'Approval'
			,	dv_incident_state as 'Ticket Status'
			,	'N/A' as 'Ticket Stage'
			,	mirror_updated_on as 'Last Update Date'
			,	dv_caller_id as 'Last Update By'
			,	resolved_at as 'Closed Date'
			,	dv_resolved_by as 'Closed By'
			,	'incident' as 'URL Helper'
			,	dv_watch_list as 'Watch List'
			from mirror.ip_group_incidents
			where Department_Name in (<DEPT>)
				and resolved_at is null
			
		union

		select	`number` as 'Ticket Number'
			,	sys_id as 'Ticket System Id'
			,	active as 'Active'
			,	Department_Name as 'Department'
			,	u_esm_function as 'Ticket Function'
			,	'Request' as 'Ticket Type'
			,	description as 'Ticket Description'
			,	u_ipg_company as 'External Company'
			,	opened_at as 'Opened Date'
			,	dv_opened_by as 'Opened By'
			,	CASE WHEN dv_assigned_to is NULL THEN 'NONE' ELSE dv_assigned_to END
			,	CASE WHEN dv_assignment_group IS NULL THEN 'NONE' ELSE dv_assignment_group END AS 'Assignment Group'
			,	approval as 'Approval'
			,	dv_state as 'Ticket Status'
			,	u_stage as 'Ticket Stage'
			,	sys_updated_on as 'Last Update Date'
			,	sys_updated_by as 'Last Update By'
			,	closed_at as 'Closed Date'
			,	dv_closed_by as 'Closed By'
			,	'u_adhoc_requests' as 'URL Helper'
			,	dv_watch_list as 'Watch List'
			from mirror.ip_group_requests
			where Department_Name in (<DEPT>)
				and active = 1 
				and dv_state not in ('Closed','Fulfilled')
				and u_esm_function <> 'Property'
				and u_stage not like '%%Close%%' and u_stage not like '%%Fulfil%%'
	'''

	db_mysql = create_engine('mysql+pymysql://eagroupread:zZjm/&UQc>@gb-cam-snowmir1.emea.arm.com:3306', echo=False)
	ticket_result = pd.read_sql(ticket_sql.replace('<DEPT>',dept_string), db_mysql)
	return ticket_result

def sortTickets(tickets, days):
	sorted_dict = {}
	for ticket in tickets.iterrows():
		days_update = (datetime.today() - ticket[1]['Last Update Date']).days
		if((date.today().weekday() < 3 and days_update - 2 >= int(days)) or days_update >= int(days)): #exclude weekend days from calc
			if(ticket[1]['Ticket Type'] not in sorted_dict.keys()):
				sorted_dict[ticket[1]['Ticket Type']] = {}
			if(ticket[1]['Ticket Function'] not in sorted_dict[ticket[1]['Ticket Type']].keys()):
				sorted_dict[ticket[1]['Ticket Type']][ticket[1]['Ticket Function']] = []
			sorted_dict[ticket[1]['Ticket Type']][ticket[1]['Ticket Function']].append(dict(ticket[1]))
	return sorted_dict

def calcTickets(div, tickets_dict):
	for ticket_type, ticket_functions in tickets_dict.items():
		for ticket_function, tickets in ticket_functions.items():
			ndx = 0
			for ticket in tickets:
				days_update = (datetime.today() - ticket['Last Update Date']).days
				days_open = (datetime.today() - ticket['Opened Date']).days
				watch_ok = watchers.calcWatcherStatus(div, ticket)
				url = 'https://arm.service-now.com/sp?id=ticket&table=' + ticket['URL Helper'] + '&sys_id=' + ticket['Ticket System Id']
				tickets_dict[ticket_type][ticket_function][ndx]['days_update'] = days_update
				tickets_dict[ticket_type][ticket_function][ndx]['days_open'] = days_open
				tickets_dict[ticket_type][ticket_function][ndx]['watch_ok'] = watch_ok
				tickets_dict[ticket_type][ticket_function][ndx]['url'] = url
				ndx = ndx + 1

	#sort arrays
	for ticket_type, ticket_functions in tickets_dict.items():
		for ticket_function, tickets in ticket_functions.items():
			sorted(tickets, key = lambda i: i['days_update'], reverse=True)
	return tickets_dict

def getTopN(ticket_result, n):
	return ticket_result.sort_values(by=['Last Update Date']).head(n)

def main(div, days):
	ticket_result = readFromDB(div)
	#sort tickets and calc metrics
	sorted_dict = sortTickets(ticket_result, days)
	calc_dict = calcTickets(div, sorted_dict)

	#write html
	htmlString = html.header.replace('<DAYS>', str(days))
	for ticketType, ticketFunctions in calc_dict.items():
		htmlString = htmlString + html.breakHeader + html.replaceWithString('<TYPE>', ticketType, html.typeheader)
		for ticketFunction, tickets in ticketFunctions.items():
			htmlString = htmlString + html.smallBreak_dark + html.replaceWithString('<FUNCTION>', ticketFunction + ' - ' + str(len(tickets)), html.functionheader)
			tickets.sort(key = lambda x: x['days_update'], reverse=True)
			htmlString = htmlString + html.tableHeader + html.buildTableLines(tickets) + "</table></td></tr>" + html.smallBreak_dark
	htmlString = htmlString + html.footer

	top10_result = getTopN(ticket_result, 10)
	sorted_top10_dict = sortTickets(top10_result, days)
	calc_top10_dict = calcTickets(div, sorted_top10_dict)

	htmlString_top10 = html.header_top10
	for ticketType, ticketFunctions in calc_top10_dict.items():
		htmlString_top10 = htmlString_top10 + html.breakHeader + html.replaceWithString('<TYPE>', ticketType, html.typeheader)
		for ticketFunction, tickets in ticketFunctions.items():
			htmlString_top10 = htmlString_top10 + html.smallBreak_dark + html.replaceWithString('<FUNCTION>', ticketFunction + ' - ' + str(len(tickets)), html.functionheader)
			htmlString_top10 = htmlString_top10 + html.tableHeader + html.buildTableLines(tickets) + "</table></td></tr>" + html.smallBreak_dark
	htmlString_top10 = htmlString_top10 + html.footer

	return htmlString, htmlString_top10


if __name__ == "__main__":
	htmlString, htmlString_top10 = main(sys.argv[1], sys.argv[2])
	titleString = 'DIV' + sys.argv[1] + ' SNOW ' + str(sys.argv[2]) + ' DAY STATUS REPORT'
	titleString_top10 = 'DIV' + sys.argv[1] + ' TOP 10 STALE SNOW TICKETS REPORT'
	mailinglist = []
	for ndx in range(3, len(sys.argv)):
		mailinglist.append(sys.argv[ndx])
	sendEmail.sendEmail(htmlString, titleString, mailinglist, 'Ops.ExceptionReports@arm.com')
	sendEmail.sendEmail(htmlString_top10, titleString_top10, mailinglist, 'Ops.ExceptionReports@arm.com')
	with open('SNOW_'+ sys.argv[1] + '_' + str(sys.argv[2]) + '.html', "w+") as f:
		f.write(htmlString)


