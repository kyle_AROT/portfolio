##################################################################################################
# purchase_order_RAG.py
# Author: Kyle Unruh, CPG Ops
# Purpose: Feeds into purchase_order_RAG.py to make the giant HTML string for the email
###################################################################################################
from datetime import datetime
import pdb


#beginning string for the html email
header = """

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

</head>
<body width="100%" bgcolor="#FFFFFF" style="margin: 0; mso-line-height-rule: exactly;">
	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->

	<!-- CSS Reset -->
	<style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
		html,
		body {
			margin: 0 auto !important;
			padding: 0 !important;
			height: 100% !important;
			width: 100% !important;
			font-family: Arial, sans-serif;
		}


		/* What it does: Stops email clients resizing small text. */
		* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}

		/* What is does: Centers email on Android 4.4 */
		div[style*="margin: 16px 0"] {
			margin:0 !important;

		}

		/* What it does: Stops Outlook from adding extra spacing to tables. */
		table,
		td {
			align: left;
			border-radius:5px;
			border:none;
		}

		/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
		table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			table-layout: fixed !important;
			margin: 0 auto !important;

		}
		table table table {
			table-layout: auto;
		}

		/* What it does: Uses a better rendering method when resizing images in IE. */
		img {
			-ms-interpolation-mode:bicubic;
		}

		/* What it does: A work-around for iOS meddling in triggered links. */
		*[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
		}

		/* What it does: A work-around for Gmail meddling in triggered links. */
		.x-gmail-data-detectors,
		.x-gmail-data-detectors *,
		.aBn {
			border-bottom: 0 !important;
			cursor: default !important;
		}

		/* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
		.a6S {
			display: none !important;
			opacity: 0.01 !important;
		}
		/* If the above doesn't work, add a .g-img class to any image in question. */
		img.g-img + div {
			display:none !important;
		}

		/* What it does: Prevents underlining the button text in Windows 10 */
		.button-link {
			text-decoration: none !important;
		}

		/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
		/* Create one of these media queries for each additional viewport size you'd like to fix */
		/* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
		@media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
			.email-container {
				min-width: 375px !important;
			}
		}


		h1 { color: #222222; font-family: 'Lato', sans-serif; font-size: 54px; font-weight: 300; line-height: 58px; margin: 0 0 58px; }
		h2 { color: #222222; font-family: 'Lato', sans-serif; font-size: 36px; font-weight: 300; line-height: 40px; margin: 0 0 40px; }


		h3 { color: #222222; font-family: 'Lato', sans-serif; font-size: 16px; line-height: 26px; text-indent: 0 px; margin: 2px; }
		
		ul,li { color: #222222; font-family: 'Lato', sans-serif; font-size: 12px; line-height: 14px; text-indent: 0px; margin: 2px; }
		p { color: #222222; font-family: 'Lato', sans-serif; font-size: 12px; line-height: 20px; margin: 2px; }


		a { font-family: 'Lato', sans-serif; color: #000000; text-decoration: underline,bold; margin: 2px;}


		a:hover { color: #ffffff }


		.date { background: #fe921f; color: #ffffff; display: inline-block; font-family: 'Lato', sans-serif; font-size: 12px; font-weight: bold; line-height: 12px; letter-spacing: 1px; margin: 0 0 30px; padding: 10px 15px 8px; text-transform: uppercase; }
	</style>

	<!-- Progressive Enhancements -->
	<style>

		/* What it does: Hover styles for buttons */
		.button-td,
		.button-a {
			transition: all 100ms ease-in;
		}
		.button-td:hover,
		.button-a:hover {
			background: #555555 !important;
			border-color: #555555 !important;
		}

	</style>

		<!--
			Set the email width. Defined in two places:
			1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
			2. MSO tags for Desktop Windows Outlook enforce a 600px width.
		-->
		<div style="max-width: 800px; margin: auto;" class="email-container">
			<!--[if mso]>
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="800" align="left">
			<tr>
			<td>
			<![endif]-->

			<!-- Email Header : BEGIN -->
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="left" width="100%" style="max-width: 800px;">
				<tr>
					<td style="padding: 20px 0; text-align: left">
						</br>
					</td>
				</tr>
			</table>
			<!-- Email Header : END -->
			<frameset cols="100,*">
			<frame name="contents" src="geoBackground-1.html"/>
			</frameset>

			<!-- Email Body : BEGIN -->
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 800px;">

				<!-- Hero Image, Flush : BEGIN -->
				<tr>
					<td style="background: transparent" align="center" >
						<h1>SNOW <DAYS>-DAY REPORT</h1>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="center" >
						<h3>All active SNOW tickets that have not been updated in at least <DAYS> days are listed below.</h3>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="center" >
						<h3>Closed, resolved, and cancelled tickets are not reported.</h3>
					</td>
				</tr>
				<!-- Hero Image, Flush : END -->
				<tr>
					<td style="background: transparent" align="left" >
						<ul>
							<li>Data source: SNOW Mirror (mysql) and SAP HANA</li>
							<li>URLs are given to access each ticket directly by clicking the ticket number</li>
							<li>When accessing a ticket from this report, the native SNOW ticket permission rules apply; only requestors and watchers can see ticket contents</li>
						</ul>
					</td>
				</tr>
"""


header_top10 = """

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

</head>
<body width="100%" bgcolor="#FFFFFF" style="margin: 0; mso-line-height-rule: exactly;">
	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->

	<!-- CSS Reset -->
	<style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
		html,
		body {
			margin: 0 auto !important;
			padding: 0 !important;
			height: 100% !important;
			width: 100% !important;
			font-family: Arial, sans-serif;
		}


		/* What it does: Stops email clients resizing small text. */
		* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}

		/* What is does: Centers email on Android 4.4 */
		div[style*="margin: 16px 0"] {
			margin:0 !important;

		}

		/* What it does: Stops Outlook from adding extra spacing to tables. */
		table,
		td {
			align: left;
			border-radius:5px;
			border:none;
		}

		/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
		table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			table-layout: fixed !important;
			margin: 0 auto !important;

		}
		table table table {
			table-layout: auto;
		}

		/* What it does: Uses a better rendering method when resizing images in IE. */
		img {
			-ms-interpolation-mode:bicubic;
		}

		/* What it does: A work-around for iOS meddling in triggered links. */
		*[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
		}

		/* What it does: A work-around for Gmail meddling in triggered links. */
		.x-gmail-data-detectors,
		.x-gmail-data-detectors *,
		.aBn {
			border-bottom: 0 !important;
			cursor: default !important;
		}

		/* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
		.a6S {
			display: none !important;
			opacity: 0.01 !important;
		}
		/* If the above doesn't work, add a .g-img class to any image in question. */
		img.g-img + div {
			display:none !important;
		}

		/* What it does: Prevents underlining the button text in Windows 10 */
		.button-link {
			text-decoration: none !important;
		}

		/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
		/* Create one of these media queries for each additional viewport size you'd like to fix */
		/* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
		@media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
			.email-container {
				min-width: 375px !important;
			}
		}


		h1 { color: #222222; font-family: 'Lato', sans-serif; font-size: 54px; font-weight: 300; line-height: 58px; margin: 0 0 58px; }
		h2 { color: #222222; font-family: 'Lato', sans-serif; font-size: 36px; font-weight: 300; line-height: 40px; margin: 0 0 40px; }


		h3 { color: #222222; font-family: 'Lato', sans-serif; font-size: 16px; line-height: 26px; text-indent: 0 px; margin: 2px; }
		
		ul,li { color: #222222; font-family: 'Lato', sans-serif; font-size: 12px; line-height: 14px; text-indent: 0px; margin: 2px; }
		p { color: #222222; font-family: 'Lato', sans-serif; font-size: 12px; line-height: 20px; margin: 2px; }


		a { font-family: 'Lato', sans-serif; color: #000000; text-decoration: underline; line-height: 20px; margin: 2px;}


		a:hover { color: #ffffff }


		.date { background: #fe921f; color: #ffffff; display: inline-block; font-family: 'Lato', sans-serif; font-size: 12px; font-weight: bold; line-height: 12px; letter-spacing: 1px; margin: 0 0 30px; padding: 10px 15px 8px; text-transform: uppercase; }
	</style>

	<!-- Progressive Enhancements -->
	<style>

		/* What it does: Hover styles for buttons */
		.button-td,
		.button-a {
			transition: all 100ms ease-in;
		}
		.button-td:hover,
		.button-a:hover {
			background: #555555 !important;
			border-color: #555555 !important;
		}

	</style>

		<!--
			Set the email width. Defined in two places:
			1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
			2. MSO tags for Desktop Windows Outlook enforce a 600px width.
		-->
		<div style="max-width: 800px; margin: auto;" class="email-container">
			<!--[if mso]>
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="800" align="left">
			<tr>
			<td>
			<![endif]-->

			<!-- Email Header : BEGIN -->
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="left" width="100%" style="max-width: 800px;">
				<tr>
					<td style="padding: 20px 0; text-align: left">
						</br>
					</td>
				</tr>
			</table>
			<!-- Email Header : END -->
			<frameset cols="100,*">
			<frame name="contents" src="geoBackground-1.html"/>
			</frameset>

			<!-- Email Body : BEGIN -->
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 800px;">

				<!-- Hero Image, Flush : BEGIN -->
				<tr>
					<td style="background: transparent" align="center" >
						<h1>SNOW TOP10 STALE TICKETS REPORT</h1>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="center" >
						<h3>This report shows the top 10 stale tickets in ServiceNow based on the last update date on active tickets</h3>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="center" >
						<h3>Closed, resolved, and cancelled tickets are not reported.</h3>
					</td>
				</tr>
				<!-- Hero Image, Flush : END -->
				<tr>
					<td style="background: transparent" align="left" >
						<ul>
							<li>Data source: SNOW Mirror (mysql) and SAP HANA</li>
							<li>URLs are given to access each ticket directly by clicking the ticket number</li>
							<li>When accessing a ticket from this report, the native SNOW ticket permission rules apply; only requestors and watchers can see ticket contents</li>
						</ul>
					</td>
				</tr>
"""

#ending part of the html email string
footer = """</td></tr></table>
			<!-- Email Body : END -->
		</div>
</body>
</html>


"""

#used to separate logical sections of the report; just a blank space 40 pixels wide
breakHeader = """
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="40" style="font-size: 0; line-height: 0;">
						&nbsp;
					</td>
				</tr>
				<!-- Clear Spacer : END -->
"""

#Smaller break than before; 10 pixels wide and colored black
smallBreak_dark = """
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="10" style="font-size: 0; line-height: 0;" bgcolor=#FFFFFF>

					</td>
				</tr>
				<!-- Clear Spacer : END -->
"""


typeheader = """
				<tr>
					<td style="background: transparent" align="left" >
						<h2><TYPE></h2>
					</td>
				</tr>
"""

functionheader = """
				<tr>
					<td style="background: transparent" align="left" >
						<h3><FUNCTION></h3>
					</td>
				</tr>
"""

tableHeader = """
				<tr>
					<td align="center" width = "100%">
						<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 800px;">
							<tr>
								<td style="background: transparent; border: 0px solid white;" align="center" width="10%">
									<p>Number</p>
								</td>
								<td style="background: transparent; border: 0px solid white;" align="center" width="30%">
									<p>Description</p>
								</td>
								<td style="background: transparent; border: 0px solid white;" align="center" width="15%">
									<p>Assigned</p>
								</td>

								<td style="background: transparent; border: 0px solid white;" align="center" width="15%">
									<p>Opened</p>
								</td>
								<td style="background: transparent; border: 0px solid white;" align="center" width="15%">
									<p>Last Update</p>
								</td>
								<td style="background: transparent; border: 0px solid white;" align="center" width="15%">
									<p>Ticket Status</p>
								</td>								
							</tr>
"""


def buildTableLines(tickets):

	tdStr_beg = "<td style=\"border: 1px solid white;\" align=\"center\" width="
	tdStr_10 = "\"10%\" "
	tdStr_15 = "\"15%\" "
	tdStr_30 = "\"30%\" "
	bgcolor_even = "bgcolor=#bfebff>"
	bgcolor_odd = "bgcolor=#7ad7ff>"
	tdStr_end = "</td>"
	
	buildString = ''
	count = 0
	for ticket in tickets:
		tdStr = "<tr>"
		bgcolor = ''
		if(count % 2 == 0):
			bgcolor = bgcolor_even
		else:
			bgcolor = bgcolor_odd

		tdStr = tdStr + tdStr_beg + tdStr_10 + bgcolor + "<p><a href=" + ticket['url'] + ">" + ticket['Ticket Number'] + "</a></p>" + tdStr_end
		tdStr = tdStr + tdStr_beg + tdStr_30 + bgcolor + "<p>" + ticket['Ticket Description'] + "</p>" + tdStr_end
		tdStr = tdStr + tdStr_beg + tdStr_15 + bgcolor + "<p>" + ticket['Assigned To'] + "</br>" + ticket['Assignment Group'] + "</p>" + tdStr_end
		tdStr = tdStr + tdStr_beg + tdStr_15 + bgcolor + "<p>" + ticket['Opened By'] + "</br>" + ticket['Opened Date'].strftime('%b %d,%Y') + " - (" + str(ticket['days_open']) + " days)</p>" + tdStr_end
		tdStr = tdStr + tdStr_beg + tdStr_15 + bgcolor + "<p>" + ticket['Last Update By'] + "</br>" + ticket['Last Update Date'].strftime('%b %d,%Y') + " - (" + str(ticket['days_update']) + " days)</p>" + tdStr_end
		tdStr = tdStr + tdStr_beg + tdStr_15 + bgcolor + "<p>" + ticket['Ticket Status'] + "</p>" + tdStr_end	
		buildString = buildString + tdStr + "</tr>"
		count = count + 1
	return buildString

#This function replaces the special substring <COUNT> in header strings with actual numbers
#If <COUNT> does not exist in the header string, nothing is changed and the string gets returned unchanged
def replaceWithCount(size, dataString):
	if '<COUNT>' in dataString:
		return dataString.replace('<COUNT>', str(size))
	return dataString

def replaceWithString(target, s, fullString):
	if target in fullString:
		return fullString.replace(target, s)
	return fullString
