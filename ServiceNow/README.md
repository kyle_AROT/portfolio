# ServiceNow

Arm uses [ServiceNow](https://www.servicenow.com/) as the company-wide tool for business groups to interact with supporting functions. ServiceNow data examples include:
	
* A group needs to hire a new employee (HR's ticket queue)
* A contract needs review (Legal's ticket queue)
* A laptop or workstation is broken and needs replacing (IT's ticket queue)

## Challenge

Sometimes, tickets take longer to complete than others or sit idle due to bottlenecks or poor communication. Staff often discovered these process breakdowns in a reactive way, causing frequent escalations to management, expensive cross-functional meetings, and many email threads. Granted, not all tickets require equal amounts of time and resources to fulfill; however, all submitted tickets are required to run the business effectively. How can Arm as an organization achieve the following with ServiceNow data:
	
* Measure the performance of active tickets
* Identify risky tickets that need extra attention to prioritize finite resources
* Understand an SLA with supporting functions based on the performance of past tickets
* Engage with this data in a self-service fashion

## Data Sources and Technology

* MySQL database - ServiceNow ticket data
* SAP Hana database - Arm organizational data
* PowerBI - visualization
* Python - data quality and targeted reporting

## Analysis and Results

First, I worked with the data engineers in IT to establish a common unique identifier across the MySQL and SAP Hana data sources. This identifier needed to pinpoint exactly one record and avoid a many-to-many data relationship. Without this identifier, the two independent data sources could not be combined. The email address field is used as the primary key across data sources as no two employees are allowed to share the same email address.

Second, I joined the MySQL and SAP Hana data together in PowerBI for a self-service dashboard. The scope of this dashboard spans both active and previously closed tickets.

**Active Tickets**

An important requirement is the ability to prioritize the tickets that need the most immediate attention. I score each ticket on a Red-Amber-Green scale based on the last update timestamp.

* RED - the last update timestamp is more than 3 weeks in the past
* AMBER - the last update timestamp is more than 1 but less than 3 weeks in the past
* GREEN - the last update timestamps is less than 1 week old

This motivates action toward red tickets and away from green tickets.

![Active Ticket Dashboard](Dashboard_demo/SNOW_active_tickets.mov)


**Closed Tickets**

Establishing a baseline service level agreement across supporting functions is not well documented at Arm. Looking at the average and distribution of past ticket performance aims to establish this baseline. 

Both average performance and distribution of performance over time are key to a well rounded analysis, so two visualizations are utilized. The first shows the trend of average ticket closure performance per month for a rolling 12-month window. Supporting this visualization is a violin plot per ticket type that aligns with previous months in scope. 


![Closed Ticket Dashboard](Dashboard_demo/SNOW_closed_tickets.mov)

Last, to round out the reporting capability of this data, Python is utilized to develop exception reports that specifically target users via regular emails on that status of open tickets. Rather than exploring the distribution or collective performance of tickets, these Python-fueled emails serve to prescribe distinct action to an intentional set of users. An example of the HTML email output is linked below, and this script runs every 3 days to the same audience, prompting frequent updates and good data quality.

[Full HTML Python Email](https://gitlab.com/kyle_AROT/portfolio/blob/master/ServiceNow/Python_reports/DIVCL_SNOW_5_DAY.pdf)

[Top 10 Python Email](https://gitlab.com/kyle_AROT/portfolio/blob/master/ServiceNow/Python_reports/DIVCL_TOP10_STALE.pdf)

