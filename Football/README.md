# Football

Without fail every Fall season, I find myself watching football games with friends and family, especially during the Thanksgiving holidays. Growing up, I always found it interesting that the commentators often disagree when comparing two football teams playing in a game. Furthermore during my college years studying data and statistics, it struck me as odd that seemingly random facts/metrics often strike up several minutes of "analysis" by the commentators.

## Challenge

Given my skepticism around the accuracy and quality of football commentary, I sought to use machine learning to predict the outcome of football games and determine which factors of team performance contribute most to predicting the winner.

## Data Sources and Technology

* Python - SVM classifier, Pandas, sklearn
* [NCAA Football data API](https://api.collegefootballdata.com/api/docs/?url=/api-docs.json)

## Analysis and Results

**Data and Normalization**

Using the NCAA football API, I pulled both season stats by team and game matchup data for over a dozen past football seasons. This resulted in hundreds of game matchups against which a model can learn classification. Each game measured the teams across 28 different features: turnovers, pass attempts, fumbles lost, punt return touch downs, passess intercepted, rushing attempts, total yards, etc. Before any analysis can occur, the scale of the different features needs to be addressed by normalizing each feature to a value between 0 and 1 per team.

The output of each game is binary: +1 if the home team wins; -1 if the away team wins

**Test Data, Train Data**

Using the sklearn Python library, I performed an 80/20 split on the game data. 80% of the data is used to train the model, and the remaining 20% test the model.

**Linear SVM Classifier**

A linear kernel support vector machine via sklearn is chosen as our classification algorithm. Linear kernel support vector machines are similar to a perceptron classification model, but a benefit of the SVM is a margin (or buffer zone) where we allow misclassifications in our training. In this case a misclassification would be saying the home team wins when the away team actually wins (the opposite case is also a misclassification).

Tolerance for these misclassifications in this application has two benefits:

* Reduces the opportunity to overfit the training data
* Helps model the "upset" football game where an underdog team rallies to defeat a feature-superior opponent

**Results**

Based on the 80/20 data split, the linear kernel SVM performed with 78.19% accuracy predicting football game outcomes. A further benefit of the linear kernel SVM is the ability to rank feature importance by weight. In other words, a higher score for a feature in the model translates to more importance for a football team to either maximize or minimize in order to win games.

The top 5 features that determine the victor of football games are:

1. Time of posession (maximize)
2. Fourth downs (minimize)
3. Kick returns forced (minimize)
4. Passing touchdowns (maximize)
5. Total yards (maximize)

If a football commentator is discussing aspects about a team matchup that do not align with one of the 5 features above, according to my model, that analysis discussion is not well correlated with the result of the game.

**2019 NCAA Bowl Game Predictions**

Using this trained model, I predicted the outcome of each bowl game coming up this December-January:

| Bowl         | Away Team | Home Team | Winner | Confidence |
| ---          | ---       | ---       | ---    | ----:      |
| Championship | Clemson | LSU | Clemson | 4.06 |
| Famous Idaho Potato | Ohio | Nevada | Ohio | 3.52 |
| Bahamas | Buffalo | Charlotte | Buffalo | 2.60 |
| Arizona | Wyoming | Georgia State | Wyoming | 2.52 |
| Sugar | Georgia | Baylor | Georgia | 2.32 |
| New Orleans | Appalachian State | UAB | Appalachian State | 2.24 |
| Cheez-It | Air Force | Washington State | Air Force | 2.22 |
| Alamo | Utah | Texas | Utah | 2.20 |
| Cure | Liberty | Georgia Southern | Liberty | 2.20 |
| Holiday | USC | Iowa | USC | 2.17 |
| Boca Raton | SMU | Florida Atlantic | SMU | 2.14 |
| Gator | Indiana | Tennessee | Indiana | 2.06 |
| LendingTree | Louisiana | Miami (OH) | Louisiana | 1.95 |
| Liberty | Navy | Kansas State | Navy | 1.87 |
| Rose | Oregon | Wisconsin | Oregon | 1.58 |
| Camellia | Florida International | Arkansas State | Arkansas State | 1.46 |
| Belk | Viginia Tech | Kentucky | Kentucky | 1.45 |
| Music City | Mississippi State | Louisville | Mississippi State | 1.38 |
| First Responder | Western Kentucky | Western Michigan | Western Kentucky | 1.26 |
| Sun | Florida State | Arizona State | Florida State | 1.25 |
| Citrus | Michigan | Alambama | Michigan | 1.10 |
| Texas | Oklahoma State | Texas A&M | Oklahoma State | 1.04 |
| Independence | Louisiana Tech | Miami | Louisiana Tech | 0.99 |
| Frisco | Utah State | Kent State | Kent State | 0.94 |
| Peach | LSU | Oklahoma | LSU | 0.77 |
| New Mexico | Central Michigan | San Diego State | Central Michigan | 0.75 |
| Redbox | California | Illinois | California | 0.66 |
| Las Vegas | Boise State | Washington | Boise State | 0.65 |
| Orange | Florida | Virginia | Florida | 0.63 |
| Pinstripe | Michigan State | Wake Forest | Wake Forest | 0.59 |
| Hawai'i | Hawai'i | BYU | Hawai'i | 0.50 |
| Outback | Minnesota | Auburn | Minnesota | 0.49 |
| Camping World | Notre Dame | Iowa State | Notre Dame | 0.49 |
| Gasparilla | UCF | Marshall | UCF | 0.43 |
| Military | North Carolina | Temple | Temple | 0.37 |
| Cotton | Penn State | Memphis | Penn State | 0.26 |
| Fiesta | Ohio State | Clemson | Clemson | 0.26 |
| Birmingham | Boston College | Cincinnati | Cincinnati | 0.15 |
| Quick Lane | Pittsburgh | Eastern Michigan | Pittsburgh | 0.07 |
| Armed Forces | Southern Mississippi | Tulane | Souther Mississippi | 0.06 | 

**Audio**

I produced a full podcast episodes for this analysis. If you're interested in listening, the Spotify link is below:

[Another Round of Thoughts](https://open.spotify.com/show/15AVd5DPk6n0aId8hfrbwv)