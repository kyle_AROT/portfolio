
from requests import request
import json
import pandas as pd
from pandas.io.json import json_normalize
import pdb
import pickle
import numpy as np
import random
import math

#scikit learn imports
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn import metrics


def buildInitialAverages():
	request_template= 'https://api.collegefootballdata.com/stats/season?year=<<YR>>'
	df_arr = []
	for yr in range(2005, 2019, 1):
		req_str = request_template.replace('<<YR>>', str(yr))
		response = request(url=req_str, method='get')
		data = response.json()
		df = pd.DataFrame()
		df = json_normalize(data)
		df_arr.append(df)

	full_df = pd.concat(df_arr, sort=False)

	full_df_pivot = full_df[['season', 'team']].drop_duplicates()
	stats = full_df.statName.unique()
	for stat in stats:
		full_df_pivot[stat] = np.nan
	for index, row in full_df.iterrows():
		full_df_pivot.loc[(full_df_pivot.season == row['season']) & (full_df_pivot.team == row['team']), row['statName']] = row['statValue']
	full_df_pivot = full_df_pivot.dropna()

	for stat in stats:
		full_df_pivot[stat+'_norm'] = 0
		for index, row in full_df_pivot.iterrows():
			stat_max = full_df_pivot[stat].max()
			stat_min = full_df_pivot[stat].min()
			full_df_pivot.loc[(full_df_pivot.season == row['season']) & (full_df_pivot.team == row['team']), stat+'_norm'] = (row[stat] - stat_min) / (float(stat_max) - stat_min)

	pdb.set_trace()
	with open("ncaa_football_team_stats.pickle", 'wb') as handle:
			pickle.dump(full_df_pivot, handle, protocol=pickle.HIGHEST_PROTOCOL)
	return full_df_pivot

def buildInitialGames():
	request_template = "https://api.collegefootballdata.com/games?year=<<YR>>"
	df_games_arr = []
	for yr in range(2005,2019,1):
		req_str = request_template.replace('<<YR>>', str(yr))
		response = request(url=req_str, method='get')
		data = response.json()
		df = pd.DataFrame()
		df = json_normalize(data)
		df = df[['season','week','home_team','home_points','away_team','away_points']]
		df = df[df.week >= 7]
		df['result'] = 1
		df.loc[df.home_points < df.away_points, 'result'] = -1
		df['spread'] = df['home_points'] - df['away_points']
		df_games_arr.append(df)
	full_df = pd.concat(df_games_arr)
	with open("ncaa_football_games_results.pickle", 'wb') as handle:
			pickle.dump(full_df, handle, protocol=pickle.HIGHEST_PROTOCOL)
	return full_df

def splitData(full_df_pivot, games_df, trainpct = 0.75):
	random.seed()
	order = random.shuffle(list(range(0, len(games_df)-1)))
	splitNdx = trainpct * len(games_df)
	count = 0

	data = pd.DataFrame()

	for row, game in games_df.iterrows():
		home_stats    = full_df_pivot.loc[(full_df_pivot.team == game['home_team']) & (full_df_pivot.season == game['season'])].filter(regex=r'_norm$').reset_index()
		away_stats    = full_df_pivot.loc[(full_df_pivot.team == game['away_team']) & (full_df_pivot.season == game['season'])].filter(regex=r'_norm$').reset_index()
		if home_stats.empty or away_stats.empty:
			continue
		feature_stats = home_stats.subtract(away_stats).iloc[0]
		feature_stats['result'] = game['result']
		data = data.append(feature_stats, ignore_index=True)

	X = data.filter(regex=r'_norm$')
	for column, values in X.iteritems():
		v_min = values.min()
		v_max = values.max()
		v = (values - v_min) / (v_max - v_min)
		X[column] = v

	Y = data['result']

	#random.seed()
	#order = list(range(0, len(data)-1))
	#random.shuffle(order)
	#splitNdx = int(trainpct * len(data))

	#train_data = []
	#train_df = pd.DataFrame(columns=data[0].index)
	#test_data  = []
	#test_df = pd.DataFrame(columns=data[0].index)
	#for i in range(len(order)):
#		if i < splitNdx:
	#		train_df = train_df.append(data[order[i]], ignore_index=True)
	#	else:
	#		test_df = test_df.append(data[order[i]], ignore_index=True)
	#train_df = pd.DataFrame.from_items(zip(train_data.index, train_data.values))
	##test_df = pd.DataFrame.from_items(zip(test_data.index, test_data.values))
	with open("X_df.pickle", 'wb') as handle:
			pickle.dump(X, handle, protocol=pickle.HIGHEST_PROTOCOL)

	with open("Y_df.pickle", 'wb') as handle:
		pickle.dump(Y, handle, protocol=pickle.HIGHEST_PROTOCOL)


	return X,Y

def svcClassifier(X_train, Y_train, X_test, Y_test):
	kernels = ['linear']
	svclassifier = ''
	for kernel in kernels:
		svclassifier = SVC(kernel=kernel)
		svclassifier.fit(X_train, Y_train)
		Y_pred = svclassifier.predict(X_test)
		feature_names = X_train.columns
		print("####################")
		print("SVC Classifier: " + kernel)
		print(metrics.accuracy_score(Y_test, Y_pred))
		feature_weights = list(zip(feature_names, svclassifier.coef_[0]))
		feature_weights.sort(key = lambda tup: tup[1])

		#print("Error: " + str(Y_err_pct))
		print("Top 5 Winning features: ")
		print(feature_weights[-5:])
		print("Top 5 Losing features: ")
		print(feature_weights[:5])
	return svclassifier




if __name__ == "__main__":
	#full_df_pivot = buildInitialAverages()
	#games_df = buildInitialGames()
	full_df_pivot = pickle.load(open("ncaa_football_team_stats.pickle", 'rb')).reset_index()
	games_df = pickle.load(open("ncaa_football_games_results.pickle", 'rb')).reset_index()
	full_df_pivot = full_df_pivot.drop(['games', 'games_norm'], axis=1)

	X,Y = splitData(full_df_pivot, games_df)
	X = pickle.load(open("X_df.pickle", 'rb'))
	Y = pickle.load(open("Y_df.pickle", 'rb'))
	X_train, X_test, Y_train, Y_test = train_test_split(X,Y, test_size = 0.20)

	svcclassifier = svcClassifier(X_train, Y_train, X_test, Y_test)
	pdb.set_trace()	
	#visualize winning game and losing game distributions
	#build new bowl matchups and see how we do!
	
	


	#visualize the ANN somehow?





