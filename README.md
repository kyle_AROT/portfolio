# Kyle Unruh

[Resume](Kyle_Unruh_resume.pdf)

## Projects
> Four data projects are included spanning across corporate requirements and personal interest.
> Each folder in this repository has a writeup + supporting files

**Corporate Projects**
* HR - measure and track hiring performance for the Infrastructure line of business at Arm
* ServiceNow - identify business execution bottlenecks at Arm using ServiceNow data across all supporting functions

**Personal Projects**
* Blackjack Simulation - explore statistical performance of common betting strategies via simulation 
* Football SVM Classifier - design a classifier to predict the victor in a NCAA football game matchup

## Supporting Links
[LinkedIN](https://www.linkedin.com/in/kyle-unruh-mba-5521b879/)

[Another Round of Thoughts - Podcast](https://www.anotherroundofthoughts.com)

