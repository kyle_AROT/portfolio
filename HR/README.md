# HR

As an analyst in the Infrastructure line of business, I am required to track and measure our hiring performance against budget, a pivotal KPI for all groups at Arm.

## Challenge

Hiring performance is key to many groups at Arm in addition to HR: finance, management, travel, operations, etc. Establishing unified insights and requirements across this diverse set of stakeholders proved challenging. After multiple weeks of cross-functional discussions, the following requirements for HR data reporting were agreed. The business questions are:

* How many people work on a team? And how many people does that team plan to hire?
* What is the likelihood that hires are going to happen on time?
* What is the headcount variance to budget forecast through the end of the fiscal year?


## Data Sources and Technology

* SAP Hana - ERP budget data, hiring status
* Sharepoint - hiring manager data
* Tableau - visualization
* Python - specialized reports and data quality

## Analysis and Results

Unlike previous projects, I identified targeted insights per stakeholder rather than create a single, self-service BI dashboard model.

**Hiring Managers & Group Managers**

Two key stakeholders are the hiring managers and group managers. These individuals are conducting interviews and selecting applicants that best form their teams for long term success. These individuals are less concerned with the inner workings of data; instead, a clear view of their teams through the end of fiscal years is prioritized. I joined the required datasets together and produced a Tableau dashboard with interactive filtering and pivoting. Status coloring on future hires help segment the likelihood for a hire to happen on time.

![Hiring Manager Dashboard](Dashboard_demo/hiring_manager_dashboard.mov)

**Finance**

The finance team has internal formulae to convert headcount numbers into direct and indirect cost on P&L statements. The valuable insight for hiring data for this set of stakeholders is the total change in headcount over time. Also, this team does not need to interact with the data often; rather, a regular snapshot in time is sufficient. As a result, I used Python to craft an automated, weekly HTML email to finance stakeholders with a moving 8-quarter window of hiring/attrition.

[Hiring Timeline Python Report - Finance](https://gitlab.com/kyle_AROT/portfolio/blob/master/HR/Python_reports/hr_timeline.pdf)

**Operations**

The operations team is responsible for ensuring alignment across all source systems of information. As a result, records at risk of slipping in terms of hiring are identified, and a prediction interval (student T, 80%, n-1 degrees of freedom) is calculated to help operations guide HR and hiring managers on more realistic start dates. Also, data quality checks happen at this level, and any record that has missing data is highlighted. Again, this report is built with Python and distributed via HTML email.

[Vacancy RAG with Predictions - Operations](https://gitlab.com/kyle_AROT/portfolio/blob/master/HR/Python_reports/hr_vacancy_RAG.pdf)


