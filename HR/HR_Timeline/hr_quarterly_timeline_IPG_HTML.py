##################################################################################################
# purchase_order_RAG.py
# Author: Kyle Unruh, CPG Ops
# Purpose: Feeds into purchase_order_RAG.py to make the giant HTML string for the email
###################################################################################################
from datetime import datetime
import pdb


#beginning string for the html email
header = """

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->
</head>
<body width="100%" style="margin: 0; mso-line-height-rule: exactly;">
	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->

	<!-- CSS Reset -->
	<style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
		html,
		body {
			margin: 0 auto !important;
			padding: 0 !important;
			height: 100% !important;
			width: 100% !important;
			font-family: Arial, sans-serif;
			bgcolor: black !important;
		}


		/* What it does: Stops email clients resizing small text. */
		* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
			box-sizing: border-box;
		}

		/* What is does: Centers email on Android 4.4 */
		div[style*="margin: 16px 0"] {
			margin:0 !important;

		}

		/* What it does: Stops Outlook from adding extra spacing to tables. */
		table,
		td {
			align: left;
			border-radius:5px;
			border:none;
		}

		/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
		table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			table-layout: fixed !important;
			margin: 0 auto !important;

		}
		table table table {
			table-layout: auto;
		}

		/* What it does: Uses a better rendering method when resizing images in IE. */
		img {
			-ms-interpolation-mode:bicubic;
		}

		/* What it does: A work-around for iOS meddling in triggered links. */
		*[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
		}

		/* What it does: A work-around for Gmail meddling in triggered links. */
		.x-gmail-data-detectors,
		.x-gmail-data-detectors *,
		.aBn {
			border-bottom: 0 !important;
			cursor: default !important;
		}

		/* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
		.a6S {
			display: none !important;
			opacity: 0.01 !important;
		}
		/* If the above doesn't work, add a .g-img class to any image in question. */
		img.g-img + div {
			display:none !important;
		}

		/* What it does: Prevents underlining the button text in Windows 10 */
		.button-link {
			text-decoration: none !important;
		}

		/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
		/* Create one of these media queries for each additional viewport size you'd like to fix */
		/* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
		@media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
			.email-container {
				min-width: 375px !important;
			}
		}


		h1 { color: #000000; font-family: 'Lato', sans-serif; font-size: 40px; font-weight: 200; line-height: 44px; margin: 0 0 2px; }
		h2 { color: #000000; font-family: 'Lato', sans-serif; font-size: 26px; font-weight: 300; line-height: 30px; margin: 0 0 2px; }


		h3 { color: #000000; font-family: 'Lato', sans-serif; font-size: 16px; line-height: 26px; text-indent: 0 px; margin: 2px; }
		
		ul,li { color: #000000; font-family: 'Lato', sans-serif; font-size: 12px; line-height: 14px; text-indent: 0px; margin: 2px; }
		p { color: #000000; font-family: 'Lato', sans-serif; font-size: 12px; line-height: 18px; margin: 2px; }


		a { font-family: 'Lato', sans-serif; color: #aaaaaa; text-decoration: underline,bold; }


		a:hover { color: #000000 }


		.date { background: #fe921f; color: #000000; display: inline-block; font-family: 'Lato', sans-serif; font-size: 12px; font-weight: bold; line-height: 12px; letter-spacing: 1px; margin: 0 0 30px; padding: 10px 15px 8px; text-transform: uppercase; }
	</style>

	<!-- Progressive Enhancements -->
	<style>

		/* What it does: Hover styles for buttons */
		.button-td,
		.button-a {
			transition: all 100ms ease-in;
		}
		.button-td:hover,
		.button-a:hover {
			background: #555555 !important;
			border-color: #555555 !important;
		}

		/* The actual timeline (the vertical ruler) */
		.timeline {
		  position: relative;
		  max-width: 1400px;
		  margin: 0 auto;
		}

		/* The actual timeline (the vertical ruler) */
		.timeline::after {
		  content: '';
		  position: absolute;
		  width: 6px;
		  background-color: grey;
		  top: 0;
		  bottom: 0;
		  left: 50%;
		  margin-left: -3px;
		}

		/* Container around content */
		.container {
		  padding: 10px 50px;
		  position: relative;
		  background-color: inherit;
		  width: 50%;
		}

		/* The circles on the timeline */
		.container::after {
		  content: '';
		  position: absolute;
		  width: 25px;
		  height: 25px;
		  right: -17px;
		  top: 15px;
		  border-radius: 50%;
		  z-index: 1;
		}

		.future::after {
		  background-color: #61D4E8;
		  border: 4px solid #2DA1E7;
		}

		.present::after {
		  background-color: #61eb61;
		  border: 4px solid #2De765;
		}

		.past::after {
		  background-color: #dedede;
		  border: 4px solid #BDBDBD;
		}

		/* Place the container to the left */
		.left {
		  left: 0;
		}

		/* Place the container to the right */
		.right {
		  left: 50%;
		}

		/* Add arrows to the left container (pointing right) */
		.left::before {
		  content: " ";
		  height: 0;
		  position: absolute;
		  top: 22px;
		  width: 0;
		  z-index: 1;
		  right: 30px;
		  border: medium solid black;
		  border-width: 10px 0 10px 10px;
		  border-color: transparent transparent transparent black;
		}

		/* Add arrows to the right container (pointing left) */
		.right::before {
		  content: " ";
		  height: 0;
		  position: absolute;
		  top: 22px;
		  width: 0;
		  z-index: 1;
		  left: 30px;
		  border: medium solid black;
		  border-width: 10px 10px 10px 0;
		  border-color: transparent black transparent transparent;
		}

		/* Fix the circle for containers on the right side */
		.right::after {
		  left: -16px;
		}

		/* The actual content */
		.content {
		  padding: 20px 30px;
		  position: relative;
		  border-radius: 6px;
		}

		.content_past {
		  background-color: #e6e6e6
		}

		.content_present {
		  background-color: #2AEB8A
		}

		.content_future {
		  background-color: #30CBFF
		}

		/* Media queries - Responsive timeline on screens less than 600px wide */
		@media screen and (max-width: 1400px) {
		/* Place the timelime to the left */
		  .timeline::after {
		    left: 31px;
		  }

		/* Full-width containers */
		  .container {
		    width: 100%;
		    padding-left: 70px;
		    padding-right: 25px;
		  }

		/* Make sure that all arrows are pointing leftwards */
		  .container::before {
		    left: 60px;
		    border: medium solid white;
		    border-width: 10px 10px 10px 0;
		    border-color: transparent white transparent transparent;
		  }

		/* Make sure all circles are at the same spot */
		  .left::after, .right::after {
		    left: 15px;
		  }

		/* Make all right containers behave like the left ones */
		  .right {
		    left: 0%;
		  }
		}

	</style>

		<!--
			Set the email width. Defined in two places:
			1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
			2. MSO tags for Desktop Windows Outlook enforce a 600px width.
		-->
		<div style="max-width:1400px; margin: auto;" class="email-container">
			<!--[if mso]>
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="1400" align="left">
			<tr>
			<td>
			<![endif]-->

			<!-- Email Header : BEGIN -->
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="left" width="100%" style="max-width: 1400px;">
				<tr>
					<td style="padding: 20px 0; text-align: left">
						</br>
					</td>
				</tr>
			</table>
			<!-- Email Header : END -->
			<frameset cols="100,*">
			<frame name="contents" src="geoBackground-1.html"/>
			</frameset>

			<!-- Email Body : BEGIN -->
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1400px;">

				<!-- Hero Image, Flush : BEGIN -->
				<tr>
					<td style="background: transparent" align="center" >
						<h1>DIVCL Headcount Timeline & Changes Report</h1>
					</td>
				</tr>
				<!-- Hero Image, Flush : END -->
				<tr>
					<td style="background: transparent" align="center" >
						<p>This report tracks all inbound and outbound orgchart changes for DIVCL cost centers.</p>
						<p>Reach out to Kyle Unruh (kyle.unruh@arm.com) for direct support on this report.</p>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="left" >
						<h4>Notes:</h4>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="left" >
						<ul>
							<li>A rolling 8 quarter view (4 quarters in the past, current quarter, 3 quarters in the future) is reported</li>
							<li>All quarters are fiscal, not calendar</li>
							<li>Total headcount at the end of the periods is given along with net change from the previous quarter in parentheses.</li>
							<li>Totals are given at the DIVCL and staff team levels.</li>
							<li>Headcount change detail is reported only at the staff team level.</li>
							<li>China JV employees are excluded.</p>
						</ul>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="left" >
						<h4>Data Sources:</h4>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="left" >
						<ul>
							<li> SAP People Data via Fusion: PeopleEvents View</li>
							<li> SharePoint List: active and placeholder budget reqs</li>
							<li> Excel: Adaptive extract of 10YP for total headcount (COMING SOON! - WIP)</li>
						</ul>
					</td>
				</tr> </td></tr>
			<tr><td>
			<div class="timeline">
"""

#ending part of the html email string
footer = """
		</div></td></tr></table>
			<!-- Email Body : END -->
		</div>
</body>
</html>


"""

container_string = """
	<div class = "container <<TIMING>> <<DIRECTION>>" >
		<div class = "content" style="background-color: <<BACKGROUND>>">
			<h2 align="center"> <<FYQ HEADER>> </h2>
			<h2 align="center"> <<TOTALS HEADER>> </h2></br>
			<div> <<TEAM DATA>> </div>
		</div>
	</div>
	</br>
	</br>
	</br>
"""

in_out_table_header = """
						<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
							<tr>
								<td style="border: 0px solid <<BACKGROUND>>;" align="center" width="50%" bgcolor= <<BACKGROUND>> >
									<p>IN (<<IN COUNT>>)</p>
								</td>
								<td style="border: 0px solid <<BACKGROUND>>;" align="center" width="50%" bgcolor= <<BACKGROUND>> >
									<p>OUT (<<OUT COUNT>>)</p>
								</td>
							</tr>
							<tr>
								<td style="border: 0px solid <<BACKGROUND>>;" align="left" width="50%" bgcolor= <<BACKGROUND>> >
									<ul style="padding:0; margin-left:15px; background:transparent">
										<<IN LIST ITEMS>>
									</ul>
								</td>
								<td style="border: 0px solid <<BACKGROUND>>;" align="left" width="50%" bgcolor= <<BACKGROUND>> >
									<ul>
										<<OUT LIST ITEMS>>
									</ul>
								</td>
							</tr>
						</table>

"""

def addQuarter(team_numbers, dir, fiscal_date, fiscal_year_qtr, runningRecord, now):
	container_type = '#e6e6e6'
	if fiscal_date == now:
		container_type = '#2AEB8A'
	elif fiscal_date > now:
		container_type = '#30CBFF'

	content_background = container_type

	direction = 'right'
	if dir % 2 == 0:
		direction = 'left'

	fyqString = ''
	totalsString = ''
	teamString = ''
	runningRecord_add = {}
	for team, data in team_numbers.items():
		teamString = teamString + "<div style=\"border: 2px solid" + content_background + ";\">"
		if team == "Total":
			if fiscal_year_qtr not in data['FYQs']:
				fyqString = fiscal_year_qtr
				totalsString = 'Beg HC: ' + str(item['Total'] - item['Net Change']) + ' | Net HC Change: ' + str(item['Net Change']) + ' | End HC: ' + str(item['Total'])
				runningRecord_add[team] = runningRecord[-1]['Total']
			else:
				for item in data['Timeline']:
					if item['FYQ'] == fiscal_year_qtr:
						fyqString = fiscal_year_qtr 
						totalsString = 'Beg HC: ' + str(item['Total'] - item['Net Change']) + ' | Net HC Change: ' + str(item['Net Change']) + ' | End HC: ' + str(item['Total'])
						runningRecord_add[team] = item['Total']

		else:
			if fiscal_year_qtr in data['FYQs']:
				for item in data['Timeline']:
					if item['FYQ'] == fiscal_year_qtr:
						teamString = teamString + "<h3 align=\"center\">" + team + "</h3>"
						teamString = teamString + '<h3 align=\"center\">Beg HC: ' + str(item['Total'] - item['Net Change']) + ' | Net HC Change: ' + str(item['Net Change']) + ' |  End HC: ' + str(item['Total']) + "</h3>"
						in_arr  = ['<li>' + v['employeename'] + ': ' + v['costcode'] + ', ' + v['typedate'].strftime("%m/%d/%y") + ', ' + v['type'] + '</li>' for v in item['In']]
						out_arr = ['<li>' + v['employeename'] + ': ' + v['costcode'] + ', ' + v['typedate'].strftime("%m/%d/%y") + ', ' + v['type'] + '</li>' for v in item['Out']]
						in_str = ''.join(in_arr)
						out_str = ''.join(out_arr)
						tableString = in_out_table_header
						tableString = tableString.replace("<<BACKGROUND>>", content_background)
						tableString = tableString.replace("<<IN LIST ITEMS>>", in_str)
						tableString = tableString.replace("<<IN COUNT>>", str(len(in_arr)))
						tableString = tableString.replace("<<OUT LIST ITEMS>>", out_str)
						tableString = tableString.replace("<<OUT COUNT>>", str(len(out_arr)))
						teamString = teamString + "</br>" + tableString
						runningRecord_add[team] = item['Total']
			else:
				teamString = teamString + "<h3>" + team + ': ' + str(runningRecord[-1][team]) + ' (' + str(0) + ') </h3>'
				runningRecord_add[team] = runningRecord[-1][team]
		teamString = teamString + "</div>"

	string = container_string.replace("<<TIMING>>", container_type)
	string = string.replace("<<BACKGROUND>>", content_background)
	string = string.replace("<<DIRECTION>>", direction)
	string = string.replace("<<FYQ HEADER>>", fyqString)
	string = string.replace("<<TOTALS HEADER>>", totalsString)
	string = string.replace("<<TEAM DATA>>", teamString)
	return string, runningRecord_add
