import csv, sys, datetime, os
from sqlalchemy import create_engine
import hr_quarterly_timeline_IPG_HTML as html
import pandas as pd
import sendEmail as sendEmail
import pyhdb
import pdb
import pickle
from shareplum import Site
from shareplum import Office365
from datetime import datetime, date
import dateutil.relativedelta

#As of FY17 Q3 starting
cc_groups_in = {
	"Mohamed": ['CNINEGSE','CNINMKNW','CNINMKSE','FRINMKHP','SVINEGSE','SVINMKEC','SVINMKGT','SVINMKHP','SVINMKNW','SVINMKSE','SWINMKNW','UKINEGSE','UKINMKEC', 'UKINMKGT', 'UKINMKHP', 'UKINMKNW', 'UKINMKSE', 'UKINOP', 'Outbound', 'GTM', 'HPC', 'Ecosystem'],
	"Dermot": ['SVINEGSO', 'UKINEGSO', 'SVINMKSO', 'UKINMKSO', 'TAINEGSO', 'Solutions - MK', 'Solutions - EG'],
	"Leeor": ['SVINOP', 'Operations']
}

cc_groups_cl = {
	"CL-LT + Ops": ['KGCLOP', 'SVCLOP', 'UKCLOP', 'UKCLOPIE'],
	"Solutions MK": ['ILCLMKSO', 'KOCLMKSO', 'SVCLMKSO','TACLMKSO', 'UKCLMKSO'],
	"Solutions EG": ['NOCLEGSO', 'UKCLEGSO'],
	"Immersive Experience": ['SVCLMKIE', 'UKCLMKIE'],
	"Marketing Programs": ['SVCLMKMP', 'TACLMKMP', 'UKCLMKMP'],
	"Product Management":['AUCLMKPD', 'FRCLMKPD', 'ILCLMKPD', 'JPCLMKPD', 'KOCLMKPD','NOCLMKPD', 'SVCLMKPD', 'TACLMKPD', 'UKCLMKPD']
}



def readFromDB():
	sql_query = '''
		SELECT COSTCODE, COUNT(*) AS "EMPLOYEE_NUM"
		FROM "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/PEOPLE"
		WHERE DIVISION = 'CL' AND EMPLOYMENTSTATUS = 'Active' AND EMPLOYEEGROUP = 'Established'
		GROUP BY COSTCODE
	'''

	changes_query = '''
		SELECT 		H.EMPLOYEENAME
				,	H.POSITIONID
				,	H.PFREQID
				,	H.POSITIONTITLE
				,	H.COSTCODE
				,	H."TYPE"
				,	H.TYPEDATE
				,	H.HEADCOUNTCALC
				,	H.DEPARTMENT
				,	CASE WHEN P.LEVEL4EMPLOYEENAME IS NULL THEN
						CASE WHEN H.DEPARTMENT = 'CLOP' THEN 'Paul Williamson'
							 WHEN H.DEPARTMENT = 'CLEG' THEN 'Laurence Bryant'
							 WHEN H.DEPARTMENT = 'CLMK' THEN 'Simon Holland'
							 ELSE 'Paul Williamson' END 
						ELSE P.LEVEL4EMPLOYEENAME END AS LEVEL4EMPLOYEENAME
				,	QUARTER(H.TYPEDATE, 4) AS "FISCAL_YEAR_QTR"
				FROM "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/HEADCOUNT" AS H
				LEFT JOIN "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/PEOPLE" AS P ON P.POSITIONID = H.POSITIONID
				WHERE 	H.DIVISION = 'CL' 
					AND H.TYPEDATE >'2017-10-01' 
					AND (H."TYPE" LIKE '%Transfer%' OR H."TYPE" LIKE '%Leaver%' OR H."TYPE" LIKE '%Start%')
					AND (H.EMPLOYEEGROUP = 'Established')
				
		UNION
		
		SELECT *,	QUARTER(TYPEDATE, 4) AS "FISCAL_YEAR_QTR" FROM
			(SELECT 	CASE WHEN H.EMPLOYEENAME IS NULL THEN H.POSITIONTITLE ELSE H.EMPLOYEENAME END AS EMPLOYEENAME
					,	H.POSITIONID
					,	H.PFREQID
					,	H.POSITIONTITLE
					,	H.COSTCODE
					,	H."TYPE"
					,	CASE WHEN PF.TARGETSTARTDATE IS NOT NULL THEN 
							CASE WHEN PF.TARGETSTARTDATE > H.TYPEDATE THEN PF.TARGETSTARTDATE
							ELSE SAP.TARGETSTARTDATE END
							ELSE SAP.TARGETSTARTDATE END AS TYPEDATE
					,	H.HEADCOUNTCALC
					,	H.DEPARTMENT
					,	CASE WHEN P.LEVEL4EMPLOYEENAME IS NULL THEN
							CASE WHEN H.DEPARTMENT = 'CLOP' THEN 'Paul Williamson'
								 WHEN H.DEPARTMENT = 'CLEG' THEN 'Laurence Bryant'
								 WHEN H.DEPARTMENT = 'CLMK' THEN 'Simon Holland'
								 ELSE 'Paul Williamson' END 
							ELSE P.LEVEL4EMPLOYEENAME END AS LEVEL4EMPLOYEENAME
					FROM "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/HEADCOUNT" AS H
					LEFT JOIN "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/PEOPLE" AS P ON P.POSITIONID = H.POSITIONID
					LEFT JOIN "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/PEOPLEFLUENTVACANCIES" AS PF ON H.POSITIONID = PF.POSITIONID
					LEFT JOIN "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/SAPVACANCIES" AS SAP ON H.POSITIONID = SAP.POSITIONID
					WHERE 	H.DIVISION = 'CL' 
						AND H.TYPEDATE >'2017-10-01' 
						AND H."TYPE" IN ('Open', 'Offers', 'Acceptances')
						AND (H.EMPLOYEEGROUP = 'Established'))
	'''
	db = create_engine('hana+pyhdb://kylunr01:InfrastructureOps2019!@fusionpro.arm.com:32515', echo=False)
	starting_result = pd.read_sql(sql_query, db)
	changes_result = pd.read_sql(changes_query, db)
	changes_result = changes_result.sort_values(by=['typedate'])

	return starting_result, changes_result



def determineTeam(costcode):
	for team, costcodes in cc_groups_cl.items():
		if costcode in costcodes:
			return team
	return "NONE"

def main():

	cc_groups_cl_pivot = {}
	for group, ccs in cc_groups_cl.items():
		for cc in ccs:
			cc_groups_cl_pivot[cc] = group

	starting, changes = readFromDB()
	team_numbers = {'Total': {"Start":0, "Timeline": [], "FYQs": set()}}
	team_set = set()
	for index,row in starting.iterrows():
		team = cc_groups_cl_pivot[row['costcode']]
		if team not in team_numbers.keys():
			team_numbers[team] = {"Start":0, "Timeline": [], "FYQs": set()}
			team_set.add(team)
		team_numbers[team]['Start'] += row['employee_num']
		team_numbers['Total']['Start'] += row['employee_num']

	vacancy_status_set = {'Open', 'Offers', 'Acceptances'}
	#1) Undo in quarter actions - get start of current quarter number
	fiscal_date = datetime.now().date()-dateutil.relativedelta.relativedelta(months=3)
	fiscal_date_num = fiscal_date.year*4 + (fiscal_date.month-1)//3 + 1
	fiscal_date_str = str(fiscal_date.year) + '-Q' + str((fiscal_date.month-1)//3 + 1)

	inqtr_df = changes[(changes.fiscal_year_qtr == fiscal_date_str) & (changes.typedate <= fiscal_date) & (~changes.type.isin(vacancy_status_set))]
	for index, row in inqtr_df.iterrows():
		team = cc_groups_cl_pivot[row['costcode']]
		team_numbers[team]['Start'] -= row['headcountcalc']
		team_numbers['Total']['Start'] -= row['headcountcalc']

	#2)Backtrack quarters
	for i in range(2,7):
		fiscal_date_num_back = fiscal_date_num - i
		fiscal_year_back = fiscal_date_num_back // 4
		fiscal_quarter_back = fiscal_date_num_back % 4 + 1
		fiscal_date_back_str = str(fiscal_year_back) + '-Q' + str(fiscal_quarter_back)

		in_arr_total = []
		out_arr_total = []

		for team in team_set:
			#pdb.set_trace()
			past_qtr_df = changes[(changes.fiscal_year_qtr == fiscal_date_back_str) & (changes.costcode.isin(cc_groups_cl[team])) & (~changes.type.isin(vacancy_status_set))]
			#pdb.set_trace()
			#team_numbers[team]["Timeline"].append({"Total": 0, "Net Change" : 0, "In": [], "Out": [], "FYQ": fiscal_date_back_str})
			in_arr = []
			out_arr = []
			for index, row in past_qtr_df.iterrows():
				if row['headcountcalc'] > 0:
					in_arr.append(row)
				else:
					out_arr.append(row)

			in_ids = set()
			out_ids = set()
			for row in in_arr:
				in_ids.add(row['positionid'])
			for row in out_arr:
				out_ids.add(row['positionid'])
			for row in in_arr:
				if(row['positionid'] in in_ids) and (row['positionid'] in out_ids):
					out_arr = [i for i in out_arr if not (i['positionid'] == row['positionid'])]
					in_arr  = [i for i in in_arr  if not (i['positionid'] == row['positionid'])] 

			net_change = len(in_arr) - len(out_arr)
			num = 0
			#pdb.set_trace()
			if len(team_numbers[team]["Timeline"]) == 0:
				num = team_numbers[team]['Start'] - net_change
			else:
				num = team_numbers[team]['Timeline'][-1]['Total'] - net_change
			team_numbers[team]["Timeline"].append({"Total": num, "Net Change" : net_change, "In": in_arr, "Out": out_arr, "FYQ": fiscal_date_back_str})
			team_numbers[team]["FYQs"].add(fiscal_date_back_str)

			in_arr_total.extend(in_arr)
			out_arr_total.extend(out_arr)
		
		net_change_total = len(in_arr_total) - len(out_arr_total)
		if len(team_numbers['Total']["Timeline"]) == 0:
			num_total = team_numbers['Total']['Start'] - net_change_total
		else:
			num_total = team_numbers['Total']['Timeline'][-1]['Total'] - net_change_total
		team_numbers['Total']["Timeline"].append({"Total": num_total, "Net Change" : net_change_total, "In": in_arr_total, "Out": out_arr_total, "FYQ": fiscal_date_back_str})
		team_numbers['Total']["FYQs"].add(fiscal_date_back_str)
		#pdb.set_trace()


	#3)look forward
	for i in range (0, 4):
		fiscal_date_num_future = fiscal_date_num + i
		fiscal_year_future = (fiscal_date_num_future - 1) // 4
		fiscal_quarter_future = (fiscal_date_num_future - 1) % 4 + 1
		fiscal_date_future_str = str(fiscal_year_future) + '-Q' + str(fiscal_quarter_future)
		
		in_arr_total = []
		out_arr_total = []

		for team in team_set:
			future_qtr_df = changes[(changes.fiscal_year_qtr == fiscal_date_future_str) & (changes.costcode.isin(cc_groups_cl[team]))]
			if i == 0:
				#pull in past vacancies
				past_vac_df = changes[(changes.typedate < date(2019, 10, 1)) & (changes.costcode.isin(cc_groups_cl[team])) & (changes.type.isin(vacancy_status_set))]
				future_qtr_df = pd.concat([future_qtr_df, past_vac_df], ignore_index=True)
				future_qtr_df = future_qtr_df.sort_values(by=['typedate'])
			in_arr = []
			out_arr = []
			for index, row in future_qtr_df.iterrows():
				if row['headcountcalc'] > 0:
					in_arr.append(row)
				else:
					out_arr.append(row)

			in_ids = set()
			out_ids = set()
			for row in in_arr:
				in_ids.add(row['positionid'])
			for row in out_arr:
				out_ids.add(row['positionid'])
			for row in in_arr:
				if(row['positionid'] in in_ids) and (row['positionid'] in out_ids):
					out_arr = [i for i in out_arr if not (i['positionid'] == row['positionid'])]
					in_arr  = [i for i in in_arr  if not (i['positionid'] == row['positionid'])] 

			net_change = len(in_arr) - len(out_arr)
			num = 0
			if i == 0:
				num = team_numbers[team]['Start'] + net_change
			else:
				num = team_numbers[team]['Timeline'][-1]['Total'] + net_change
			team_numbers[team]["Timeline"].append({"Total": num, "Net Change" : net_change, "In": in_arr, "Out": out_arr, "FYQ": fiscal_date_future_str})
			team_numbers[team]["FYQs"].add(fiscal_date_future_str)

			in_arr_total.extend(in_arr)
			out_arr_total.extend(out_arr)
		
		net_change_total = len(in_arr_total) - len(out_arr_total)
		if i == 0:
			num_total = team_numbers['Total']['Start'] + net_change_total
		else:
			num_total = team_numbers['Total']['Timeline'][-1]['Total'] + net_change_total
		team_numbers['Total']["Timeline"].append({"Total": num_total, "Net Change" : net_change_total, "In": in_arr_total, "Out": out_arr_total, "FYQ": fiscal_date_future_str})
		team_numbers['Total']["FYQs"].add(fiscal_date_future_str)

	#pdb.set_trace()

	#find index for current quarter
	fiscal_date_report_start = datetime.now().date() - dateutil.relativedelta.relativedelta(months=15)
	fiscal_year_qtr_report = str(fiscal_date_report_start.year) + '-Q' + str((fiscal_date_report_start.month - 1)//3 + 1)
	#pdb.set_trace()

	#build HTML
	htmlString = html.header

	#sort team data
	for team, data in team_numbers.items():
		team_numbers[team]['Timeline'] = sorted(data['Timeline'], key = lambda i: i['FYQ'])

	#adjust past totals
	for team, data in team_numbers.items():
		for i in range(1,5):
			team_numbers[team]['Timeline'][i]['Total'] += data['Timeline'][i]['Net Change']

	#build totals
	for fyq in team_numbers['Total']['Timeline']:
		print(fyq['FYQ'] + ' - ' + str(fyq['Total']))

	#pdb.set_trace()
	runningRecord = []
	for i in range(1, len(team_numbers['Total']['Timeline'])):
		fiscal_date = fiscal_date_report_start + dateutil.relativedelta.relativedelta(months=(i-1)*3)
		fiscal_year_qtr = str(fiscal_date.year) + '-Q' + str((fiscal_date.month - 1)//3 + 1)
		htmlString_add, runningRecord_add = html.addQuarter(team_numbers, i, fiscal_date, fiscal_year_qtr, runningRecord, datetime.now().date()-dateutil.relativedelta.relativedelta(months=3))
		htmlString = htmlString + htmlString_add
		runningRecord.append(runningRecord_add)
	htmlString = htmlString + html.footer

	sendEmail.sendEmail(htmlString, "DIVCL Headcount Timeline & Changes Report", ['kyle.unruh@arm.com', 'mahima.bulusu@arm.com', 'jonathan.toynton@arm.com'], 'Ops.ExceptionReports@arm.com')


if __name__ == "__main__":
	main()
