import csv, sys, datetime, os
from sqlalchemy import create_engine
import hr_quarterly_timeline_HTML as html
import pandas as pd
import sendEmail as sendEmail
import pyhdb
import pdb
import pickle
from shareplum import Site
from shareplum import Office365
from datetime import datetime
import dateutil.relativedelta

#As of FY17 Q3 starting
cc_groups = {
	"Mohamed": ['CNINEGSE','CNINMKNW','CNINMKSE','FRINMKHP','SVINEGSE','SVINMKEC','SVINMKGT','SVINMKHP','SVINMKNW','SVINMKSE','SWINMKNW','UKINEGSE','UKINMKEC', 'UKINMKGT', 'UKINMKHP', 'UKINMKNW', 'UKINMKSE', 'UKINOP', 'Outbound', 'GTM', 'HPC', 'Ecosystem'],
	"Dermot": ['SVINEGSO', 'UKINEGSO', 'SVINMKSO', 'UKINMKSO', 'TAINEGSO', 'Solutions - MK', 'Solutions - EG'],
	"Leeor": ['SVINOP', 'Operations']
}



def readFromDB():
	sql_query = '''
	SELECT COSTCODE , COUNT(*) AS "NUM"
		FROM "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/PEOPLEEVENTS"
		WHERE DIVISION = 'IN' AND "TYPE" in ('TransfersIn', 'Starters') AND TYPEDATE = '2017-10-01' AND EMPLOYEEGROUP = 'Established'
		GROUP BY COSTCODE
	'''

	changes_query = '''
		SELECT 		EMPLOYEENAME
				,	POSITIONID
				,	PFREQID
				,	POSITIONTITLE
				,	COSTCODE
				,	"TYPE"
				,	TYPEDATE
				,	HEADCOUNTCALC
				,	QUARTER(TYPEDATE, 4) AS "FISCAL_YEAR_QTR"
		FROM "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/HEADCOUNT" 
		WHERE 	DIVISION = 'IN' 
			AND TYPEDATE >'2017-10-01' 
			AND ("TYPE" LIKE '%Transfer%' OR "TYPE" LIKE '%Leaver%' OR "TYPE" LIKE '%Start%')
			AND (EMPLOYEEGROUP = 'Established')
		ORDER BY TYPEDATE ASC
	'''

	sap_vacancy_query = '''
		SELECT 		CASE WHEN EMPLOYEENAME IS NULL THEN POSITIONTITLE ELSE EMPLOYEENAME END AS "EMPLOYEENAME"
				,	POSITIONID
				,	PFREQID
				,	POSITIONTITLE
				,	COSTCODE
				,	"TYPE"
				,	TYPEDATE
				,	HEADCOUNTCALC
				,	QUARTER(TYPEDATE, 4) AS "FISCAL_YEAR_QTR"
		FROM "_SYS_BIC"."ARM.MODEL.VIEW.PEOPLE/HEADCOUNT" 
		WHERE 	DIVISION = 'IN' 
			AND TYPEDATE >='2018-10-01' 
			AND "TYPE" IN ('Open', 'Offers', 'Acceptances')
			AND (EMPLOYEEGROUP = 'Established')
		ORDER BY TYPEDATE ASC
	'''

	db = create_engine('hana+pyhdb://kylunr01:InfrastructureOps2019!@fusionpro.arm.com:32515', echo=False)
	starting_result = pd.read_sql(sql_query, db)
	changes_result = pd.read_sql(changes_query, db)
	sap_vacancy_result = pd.read_sql(sap_vacancy_query, db)
	#pdb.set_trace()


	sapIDs = set()
	for index, row in changes_result.iterrows():
		sapIDs.add(row['positionid'])

	#connect to sharepoint
	authcookie = Office365('https://armh.sharepoint.com', username='kyle.unruh@arm.com', password='Saxophone2020armaustin').GetCookies()
	site = Site('https://armh.sharepoint.com/sites/infrastructure/operations/hr', authcookie=authcookie)
	sp_list = site.List('Vacancies')
	sp_data = sp_list.GetListItems('datasheet', rowlimit=1000)

	err_string = ''

	for vacancy in sp_data:
		if vacancy['Ignore/Cancel?'] == 'No' and vacancy['Grade'][0] != '0' and vacancy['Started?'] == 'No':
			if 'SAP Code' not in vacancy.keys():
					#budget req, take all data from SP list
					fiscal_date = vacancy['Target Start Date'] - dateutil.relativedelta.relativedelta(months=3)
					changes_result.loc[len(changes_result)] = [vacancy['Position Title'],'-1','-1',vacancy['Position Title'], vacancy['Team'], 'Budget', vacancy['Target Start Date'].date(), 1, str(fiscal_date.year) + '-Q' + str((fiscal_date.month - 1)//3 + 1)]
			elif vacancy['SAP Code'] not in sapIDs:
				if sap_vacancy_result['positionid'].astype(str).str.contains(vacancy['SAP Code']).any():
					fiscal_date = vacancy['Target Start Date'] - dateutil.relativedelta.relativedelta(months=3)
					#take CC from SAP
					row = sap_vacancy_result.loc[sap_vacancy_result['positionid'] == vacancy['SAP Code']].iloc[0]
					changes_result.loc[len(changes_result)] = [row['employeename'],vacancy['SAP Code'],row['pfreqid'],vacancy['Position Title'], vacancy['Team'], row['type'], vacancy['Target Start Date'].date(), 1, str(fiscal_date.year) + '-Q' + str((fiscal_date.month - 1)//3 + 1)]
				else:
					#throw error. There's a vacancy not coming through in SAP
					fiscal_date = vacancy['Target Start Date'] - dateutil.relativedelta.relativedelta(months=3)
					changes_result.loc[len(changes_result)] = [vacancy['Position Title'],vacancy['SAP Code'],row['pfreqid'],vacancy['Position Title'], vacancy['Team'], 'Open', vacancy['Target Start Date'].date(), 1, str(fiscal_date.year) + '-Q' + str((fiscal_date.month - 1)//3 + 1)]
					print("Warning: sap code in SP that isn't in SAP")
					print(vacancy)
					
			else:
				#throw error. There's a vacancy not coming through in SAP
				err_string = err_string + "\nError: sap code in SP that isn't in SAP Vacancy. Check if this person is a starter!!\n"
				err_string = err_string + str(vacancy)

	changes_result = changes_result.sort_values(by=['typedate'])
	return starting_result, changes_result, err_string



def determineTeam(costcode):
	if costcode in cc_groups['Mohamed']:
		return 'Mohamed'
	elif costcode in cc_groups['Dermot']:
		return 'Dermot'
	else:
		return 'Leeor'

def main():
	team_numbers = {
		"Mohamed": {"Start":0, "Timeline": [], "FYQs": set()},
		"Dermot": {"Start":0, "Timeline": [], "FYQs": set()},
		"Leeor":{"Start":0, "Timeline": [], "FYQs": set()},
		"Total": {"Start":0, "Timeline": [], "FYQs": set()},
	}

	startedSAPCodes = set()

	#get all historical moves (SAP People Fusion)
	starting, changes, err = readFromDB()

	if err != '':
		sendEmail.sendEmail(err, "ERR: DIVIN Headcount Timeline & Changes Report", ['kyle.unruh@arm.com'], 'Ops.ExceptionReports@arm.com')
		sys.exit(1)


	for index, row in starting.iterrows():
		team = determineTeam(row['costcode'])
		team_numbers[team]['Start'] += row['num']
		team_numbers['Total']['Start'] += row['num']


	for index, row in changes.iterrows():
		#determine the team, track set of SAP codes
		startedSAPCodes.add(row['positionid'])
		team = determineTeam(row['costcode'])

		#if needed, initialize the array with index 0 element
		if row['fiscal_year_qtr'] not in team_numbers[team]["FYQs"]:
			if len(team_numbers[team]["Timeline"]) == 0:
				team_numbers[team]["Timeline"].append({"Total": team_numbers[team]["Start"], "Net Change": 0, "In": [], "Out":[], "FYQ": row['fiscal_year_qtr']})
			else:
				team_numbers[team]["Timeline"].append({"Total": team_numbers[team]["Timeline"][-1]["Total"], "Net Change": 0, "In": [], "Out":[], "FYQ": row['fiscal_year_qtr']})
			team_numbers[team]["FYQs"].add(row['fiscal_year_qtr'])

		inc = 0
		if row['headcountcalc'] > 0 or row['type'] in ('Starters', 'TransfersIn'):
			team_numbers[team]["Timeline"][-1]["In"].append(row)
			inc = 1
		elif row['headcountcalc'] < 0 or row['type'] in ('Leavers', 'Future Leavers'):
			team_numbers[team]["Timeline"][-1]["Out"].append(row)
			inc = -1
		else:
			print("Error: Type recorded without increment depicted")
			print(row)
			sys.exit(1)

		team_numbers[team]["Timeline"][-1]["Total"] += inc
		team_numbers[team]["Timeline"][-1]["Net Change"] += inc

		#do totals too. A lot of repeated code, but we need to do this for all records regardless of team
		if row['fiscal_year_qtr'] not in team_numbers['Total']["FYQs"]:
			if len(team_numbers['Total']["Timeline"]) == 0:
				team_numbers['Total']["Timeline"].append({"Total": team_numbers['Total']["Start"], "Net Change": 0, "In": [], "Out":[], "FYQ": row['fiscal_year_qtr']})
			else:
				team_numbers['Total']["Timeline"].append({"Total": team_numbers['Total']["Timeline"][-1]["Total"], "Net Change": 0, "In": [], "Out":[], "FYQ": row['fiscal_year_qtr']})
			team_numbers['Total']["FYQs"].add(row['fiscal_year_qtr'])

		inc = 0
		if row['headcountcalc'] > 0 or row['type'] in ('Starters', 'TransfersIn'):
			team_numbers['Total']["Timeline"][-1]["In"].append(row)
			inc = 1
		elif row['headcountcalc'] < 0 or row['type'] in ('Leavers','Future Leavers'):
			team_numbers['Total']["Timeline"][-1]["Out"].append(row)
			inc = -1
		else:
			print("Error: Type recorded without increment depicted")
			print(row)
			sys.exit(1)

		team_numbers['Total']["Timeline"][-1]["Total"] += inc
		team_numbers['Total']["Timeline"][-1]["Net Change"] += inc

	for team, data in team_numbers.items():
		idx = 0
		for qtr_data in data ['Timeline']:
			in_ids = set()
			out_ids = set()
			for row in qtr_data['In']:
				in_ids.add(row['positionid'])
			for row in qtr_data['Out']:
				out_ids.add(row['positionid'])

			for row in qtr_data['In']:
				if (row['positionid'] in in_ids) and (row['positionid'] in out_ids):
					team_numbers[team]['Timeline'][idx]['Out'] = [i for i in team_numbers[team]['Timeline'][idx]['Out'] if not (i['positionid'] == row['positionid'] and i['type'] == 'TransfersOut')]
					team_numbers[team]['Timeline'][idx]['In'] = [i for i in team_numbers[team]['Timeline'][idx]['In'] if not (i['positionid'] == row['positionid'] and i['type'] == 'TransfersIn')]
			idx += 1

	#find index for current quarter
	fiscal_date_report_start = datetime.now().date() - dateutil.relativedelta.relativedelta(months=15)
	fiscal_year_qtr_report = str(fiscal_date_report_start.year) + '-Q' + str((fiscal_date_report_start.month - 1)//3 + 1)
	#pdb.set_trace()

	#build HTML
	htmlString = html.header


	#build totals
	for fyq in team_numbers['Total']['Timeline']:
		print(fyq['FYQ'] + ' - ' + str(fyq['Total']))
	#pdb.set_trace()
	runningRecord = []
	for i in range(8):
		fiscal_date = fiscal_date_report_start + dateutil.relativedelta.relativedelta(months=i*3)
		fiscal_year_qtr = str(fiscal_date.year) + '-Q' + str((fiscal_date.month - 1)//3 + 1)
		htmlString_add, runningRecord_add = html.addQuarter(team_numbers, i, fiscal_date, fiscal_year_qtr, runningRecord, datetime.now().date()-dateutil.relativedelta.relativedelta(months=3))
		htmlString = htmlString + htmlString_add
		runningRecord.append(runningRecord_add)
	htmlString = htmlString + html.footer

	sendEmail.sendEmail(htmlString, "DIVIN Headcount Timeline & Changes Report", ['kyle.unruh@arm.com', 'chris.bergey@arm.com', 'mohamed.awad@arm.com', 'dermot.odriscoll@arm.com', 'leeor.mamou@arm.com', 'mark.glenewinkel@arm.com', 'nan.wang@arm.com', 'anne.grassian@arm.com'], 'Ops.ExceptionReports@arm.com')
	with open("test.html", "w") as f:
		f.write(htmlString)

	#sendEmail.sendEmail(htmlString, "DIVIN Headcount Timeline & Changes Report", ['kyle.unruh@arm.com'], 'Ops.ExceptionReports@arm.com')

if __name__ == "__main__":
	main()
