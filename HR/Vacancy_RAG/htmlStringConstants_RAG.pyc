�
�{`[c           @   s�   d  d l  Z  d  d l Z d  d l Z d  d l Z d Z d Z d Z d Z d Z d Z	 d Z
 d	 Z d
 Z d Z d Z d Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d S(   i����Ns+  

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
	<meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

	<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
	<!--[if mso]>
		<style>
			* {
				font-family: sans-serif !important;
			}
		</style>
	<![endif]-->

	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->

	<!-- CSS Reset -->
	<style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
		html,
		body {
			margin: 0 auto !important;
			padding: 0 !important;
			height: 100% !important;
			width: 100% !important;
		}

		/* What it does: Stops email clients resizing small text. */
		* {
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
		}

		/* What is does: Centers email on Android 4.4 */
		div[style*="margin: 16px 0"] {
			margin:0 !important;

		}

		/* What it does: Stops Outlook from adding extra spacing to tables. */
		table,
		td {
			mso-table-lspace: 0pt !important;
			mso-table-rspace: 0pt !important;
			align: center;
			border-radius:5px;
			border:none;
		}

		/* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
		table {
			border-spacing: 0 !important;
			border-collapse: collapse !important;
			table-layout: fixed !important;
			margin: 0 auto !important;

		}
		table table table {
			table-layout: auto;
		}

		/* What it does: Uses a better rendering method when resizing images in IE. */
		img {
			-ms-interpolation-mode:bicubic;
		}

		/* What it does: A work-around for iOS meddling in triggered links. */
		*[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
		}

		/* What it does: A work-around for Gmail meddling in triggered links. */
		.x-gmail-data-detectors,
		.x-gmail-data-detectors *,
		.aBn {
			border-bottom: 0 !important;
			cursor: default !important;
		}

		/* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
		.a6S {
			display: none !important;
			opacity: 0.01 !important;
		}
		/* If the above doesn't work, add a .g-img class to any image in question. */
		img.g-img + div {
			display:none !important;
		}

		/* What it does: Prevents underlining the button text in Windows 10 */
		.button-link {
			text-decoration: none !important;
		}

		/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
		/* Create one of these media queries for each additional viewport size you'd like to fix */
		/* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
		@media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
			.email-container {
				min-width: 375px !important;
			}
		}


		h1 { color: #222222; font-family: 'Lato', sans-serif; font-size: 54px; font-weight: 300; line-height: 58px; margin: 0 0 58px; }
		h2 { color: #222222; font-family: 'Lato', sans-serif; font-size: 36px; font-weight: 300; line-height: 40px; margin: 0 0 40px; }


		h3 { color: #222222; font-family: 'Lato', sans-serif; font-size: 16px; line-height: 26px; text-indent: 0 px; margin: 2px; }
		
		p,ul,li { color: #222222; font-family: 'Lato', sans-serif; font-size: 12px; line-height: 14px; text-indent: 0px; margin: 2px; }


		a { color: #fe921f; text-decoration: underline; }


		a:hover { color: #ffffff }


		.date { background: #fe921f; color: #ffffff; display: inline-block; font-family: 'Lato', sans-serif; font-size: 12px; font-weight: bold; line-height: 12px; letter-spacing: 1px; margin: 0 0 30px; padding: 10px 15px 8px; text-transform: uppercase; }

	</style>

	<!-- Progressive Enhancements -->
	<style>

		/* What it does: Hover styles for buttons */
		.button-td,
		.button-a {
			transition: all 100ms ease-in;
		}
		.button-td:hover,
		.button-a:hover {
			background: #555555 !important;
			border-color: #555555 !important;
		}

	</style>

</head>
<body width="100%" bgcolor="#FFFFFF" style="margin: 0; mso-line-height-rule: exactly;">
	<center style="width: 100%; background: #FFFFFF; text-align: left;">

		<!--
			Set the email width. Defined in two places:
			1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
			2. MSO tags for Desktop Windows Outlook enforce a 600px width.
		-->
		<div style="max-width: 1000px; margin: auto;" class="email-container">
			<!--[if mso]>
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="800" align="center">
			<tr>
			<td>
			<![endif]-->

			<!-- Email Header : BEGIN -->
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1000px;">
				<tr>
					<td style="padding: 20px 0; text-align: center">
						</br>
					</td>
				</tr>
			</table>
			<!-- Email Header : END -->

			<!-- Email Body : BEGIN -->
			<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1000px;">

				<!-- Hero Image, Flush : BEGIN -->
				<tr>
					<td style="background: transparent" align="center" >
						<h1>Vacancy RAG Report</h1>
					</td>
				</tr>
				<!-- Hero Image, Flush : END -->

s  
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="40" style="font-size: 0; line-height: 0;">
						&nbsp;
					</td>
				</tr>
				<!-- Clear Spacer : END -->
				<tr>
					<td style="background: transparent" align="left" >
						<h3>Red - Action: need immediate attention</h3>
					</td>
				</tr>

				<tr>
					<td style="background: transparent" align="left" >
						<p>Red if any of the following are met:<p>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="left" >
						<ul>
						<li>Start date exists in the past</li>
						<li>PF ID is missing, and record is in the time to hire window (based on average TTH per location)</li>
						<li>The record is missing key pieces of information</li>
						</ul>
					</td>
				</tr>

s�   

				<tr>
					<td style="background: transparent" align="left" >
						<h3>Amber - Be Aware: on track, but should be filled in 30 days.</h3>
					</td>
				</tr>
s�  
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="40" style="font-size: 0; line-height: 0;">
						&nbsp;
					</td>
				</tr>
				<!-- Clear Spacer : END -->

				<tr>
					<td style="background: transparent" align="left" >
						<h3>Red-Amber Numbers by Location</h3>
					</td>
				</tr>
				<tr>
					<td align="center" width = "100%">
						<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1000px;">
							<tr>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>Location</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>Red Records</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>% Red</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>Amber Records</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>% Amber</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>Total Number Open Positions</p>
								</td>
							</tr>

sJ  
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="40" style="font-size: 0; line-height: 0;">
						&nbsp;
					</td>
				</tr>
				<!-- Clear Spacer : END -->

				<tr>
					<td style="background: transparent" align="left" >
						<h3>Red-Amber Numbers by Hiring Manager</h3>
					</td>
				</tr>
				<tr>
					<td style="background: transparent" align="left" >
						<p>Hiring managers with neither red nor amber records are excluded<p>
					</td>
				</tr>
				<tr>
					<td align="center" width = "100%">
						<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1000px;">
							<tr>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>Hiring Manager</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>Red Records</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>% Red</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>Amber Records</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>% Amber</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="16.66666666666%">
									<p>Total Number Open Positions</p>
							</tr>

s@  
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="40" style="font-size: 0; line-height: 0;">
						&nbsp;
					</td>
				</tr>
				<!-- Clear Spacer : END -->

				<tr>
					<td style="background: transparent" align="left" >
						<h3>Records with key data missing</h3>
					</td>
				</tr>
				<tr>
					<td align="center" width = "100%">
						<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1000px;">
							<tr>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="25%">
									<p>SAP ID</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="25%">
									<p>Position Title</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="25%">
									<p>Location</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="25%">
									<p>Hiring Manager</p>
								</td>
							</tr>
sv  
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="40" style="font-size: 0; line-height: 0;">
						&nbsp;
					</td>
				</tr>
				<!-- Clear Spacer : END -->

				<tr>
					<td style="background: transparent" align="left" >
						<h3>Start dates in the past</h3>
					</td>
				</tr>

				<tr>
					<td align="center" width = "100%">
						<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1000px;">
							<tr>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>SAP ID</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Position Title</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Type</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Location</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Hiring Manager</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Recruiter</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Target Start Date</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>PF ID</p>
								</td>
							</tr>

s_  
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="40" style="font-size: 0; line-height: 0;">
						&nbsp;
					</td>
				</tr>
				<!-- Clear Spacer : END -->

				<tr>
					<td style="background: transparent" align="left" >
						<h3>No PF ID but within TTH Window</h3>
					</td>
				</tr>

				<tr>
					<td align="center" width = "100%">
						<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1000px;">
							<tr>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="14.28571428571429%">
									<p>SAP ID</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="14.28571428571429%">
									<p>Position Title</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="14.28571428571429%">
									<p>Type</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="11.28571428571429%">
									<p>Location</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="14.28571428571429%">
									<p>Hiring Manager</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="14.28571428571429%">
									<p>Target Start Date</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="17.28571428571429%">
									<p>Predicted Hire Date Range</p>
								</td>
							</tr>

s�  
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="40" style="font-size: 0; line-height: 0;">
						&nbsp;
					</td>
				</tr>
				<!-- Clear Spacer : END -->

				<tr>
					<td style="background: transparent" align="left" >
						<h3>All red and amber open positions for CPG (ordered by start date)</h3>
					</td>
				</tr>

				<tr>
					<td align="center" width = "100%">
						<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1000px;">
							<tr>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>SAP ID</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Position Title</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Type</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Location</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Hiring Manager</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Recruiter</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Target Start Date</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>PF ID</p>
								</td>
							</tr>

sy  
				<!-- Clear Spacer : BEGIN -->
				<tr>
					<td height="40" style="font-size: 0; line-height: 0;">
						&nbsp;
					</td>
				</tr>
				<!-- Clear Spacer : END -->

				<tr>
					<td style="background: transparent" align="left" >
						<h3>Amber open positions</h3>
					</td>
				</tr>

				<tr>
					<td align="center" width = "100%">
						<table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 1000px;">
							<tr>
								<td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>SAP ID</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Position Title</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Type</p>
								</td>								
								<td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Location</p>
								</td>
								<td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Hiring Manager</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Recruiter</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>Target Start Date</p>
								</td>
								 <td style="background: transparent; border: 1px solid white;" align="center" width="12.5%">
									<p>PF ID</p>
								</td>
							</tr>

sv   
						</table>
					</td>
				</tr>


			</table>
			<!-- Email Body : END -->
		</div>
	</center>
</body>
</html>


s   
 </table></td></tr>
c         C   s�  d } d } d } d } |  d r+ d } n  d j  g  |  d D]$ } t | � d k  rZ | n d	 ^ q< � } | | | t t |  d
 � � | } | | | | | } | | | t |  d � | } |  d r� | | | |  d | } n | | | d | } |  d r)| | | |  d | } n | | | d | } |  d rf| | | |  d | } n | | | d | } | | | t |  d � | } |  d r�| | | t |  d � | } n | | | d | } d | d S(   Nt    sJ   <td style="border: 1px solid white;" align="center" width="12.5%" bgcolor=s	   </p></td>s   #FF7A7A><p>t   AFLAGs   #FFE27A><p>t   positiontitlei�   t   -t
   positionidt   typet   personnelsubareat   Nonet   managernamet	   recruitert   typedatet   pfreqids   <tr>s   </tr>(   t   joint   ordt   strt   int(   t   recordt
   totalTdStrt   tdStrBegt   tdStrEndt   bgcolort   it   newPosTitle(    (    sS   /home/kylunr01/Documents/Nitro/Autoreporting/Vacancy_RAG/htmlStringConstants_RAG.pyt	   construct6  s0    
	>& 


 
#c         C   sV  | d t  | d � d } | d t  | d � d } | d k sP | d k rNd } d } d } d	 } d
 } d }	 d }
 d j | � d } d j | � d } | | | |  | } | | | t | d � | } | | | | | } | | |	 t | d � | } | | |	 | | } | | |
 t | d � | } d | d Sd Sd  S(   Nt   redt   totalid   t   amberi    R    sM   <td style="border: 1px solid white;" align="center" width="16.6666666666666%"s	   </p></td>s   bgcolor= #a9aeb0><p>s   bgcolor=#FF7A7A><p>s   bgcolor=#FFE27A><p>s   bgcolor=#91d1ff><p>s   {0:.1f}t   %s   <tr>s   </tr>(   t   floatt   formatR   (   t   primaryt   totalst   redpctt   amberpctR   R   R   t   primaryBGStringt   redBGStringt   amberBGStringt   totalBGString(    (    sS   /home/kylunr01/Documents/Nitro/Autoreporting/Vacancy_RAG/htmlStringConstants_RAG.pyt   formatRAGRollup_Manager\  s(       c         C   s�  | d t  | d � d } | d t  | d � d } d } d } d } d } d	 } d
 }	 d }
 d j | � d } d j | � d } |  d k r� | | | d | } n; |  d k r� | | | d | } n | | | |  | } | | | t | d � | } | | | | | } | | |	 t | d � | } | | |	 | | } | | |
 t | d � | } d | d S(   NR   R   id   R   R    sM   <td style="border: 1px solid white;" align="center" width="16.6666666666666%"s	   </p></td>s   bgcolor= #a9aeb0><p>s   bgcolor=#FF7A7A><p>s   bgcolor=#FFE27A><p>s   bgcolor=#91d1ff><p>s   {0:.1f}R   t   Sans   San Joset   Kfars   Kfar Netters   <tr>s   </tr>(   R   R   R   (   R   R   R    R!   R   R   R   R"   R#   R$   R%   (    (    sS   /home/kylunr01/Documents/Nitro/Autoreporting/Vacancy_RAG/htmlStringConstants_RAG.pyt   formatRAGRollup_Locationv  s,       c         C   sN  d } d } d } d } |  d r+ d } n  d j  g  |  d D]$ } t | � d k  rZ | n d	 ^ q< � } | | | t t |  d
 � � | } |  d r� | | | | | } n | | | d | } |  d r� | | | |  d | } n | | | d | } |  d r,| | | |  d | } n | | | d | } d | d S(   NR    sH   <td style="border: 1px solid white;" align="center" width="25%" bgcolor=s	   </p></td>s   #FF7A7A><p>R   s   #FFE27A><p>R   i�   R   R   s   NO POSITION TITLER   s   NO LOCATIONR   s   NO HIRING MANAGERs   <tr>s   </tr>(   R   R   R   R   (   R   R   R   R   R   R   R   (    (    sS   /home/kylunr01/Documents/Nitro/Autoreporting/Vacancy_RAG/htmlStringConstants_RAG.pyt   addMissingHiringManager�  s$    
	>&


c         C   s�  d } d } d } d } d } d } d } d } |  d d	 d
 k rM d } n0 |  d d	 j  d � d |  d d j  d � } d j g  |  d D]$ }	 t |	 � d k  r� |	 n d ^ q� � }
 | | | t t |  d � � | } | | | |
 | } | | | t |  d � | } | | | |  d | } |  d sT| | | d | } n | | | |  d | } | | | t |  d � | } | | | | | } d | d S(   NR    sW   <td style="border: 1px solid white;" align="center" width="14.28571428571429%" bgcolor=sW   <td style="border: 1px solid white;" align="center" width="17.28571428571429%" bgcolor=sW   <td style="border: 1px solid white;" align="center" width="11.28571428571429%" bgcolor=s	   </p></td>s   #FF7A7A><p>s   #91d1ff><p>t	   DATERANGEt   lowert   nones5   not enough records to define an accurate distributions   %Y-%m-%ds        to     t   upperR   i�   R   R   R   R   R   R   R
   s   <tr>s   </tr>(   t   strftimeR   R   R   R   (   R   R   R   t   tdStrBeg_longt   tdStrBeg_shortR   R   t   rangeBGStringt
   dateStringR   R   (    (    sS   /home/kylunr01/Documents/Nitro/Autoreporting/Vacancy_RAG/htmlStringConstants_RAG.pyt   noPFIDWindow_function�  s,    	0>& 
 c          C   s�   d }  t  j t  j �  � d } t  j | � d } t d d � � } | j �  } Wd  QX| j d d � d } | j d d � d	 } |  S(
   Ns�   
				<tr>
					<td style="background: transparent" align="left" >
						<h3>8 Week Red/Amber Movement Per Location - see attached PDF file</h3>
					</td>
				</tr>
				s%   sc_html.exe -f eightWeeksRedAmber.bmpR    s   eightWeeksRedAmber.htmlt   rs   <body>i   s   </body>i    (   t   ost   chdirt   getcwdt   systemt   opent   readt   split(   t
   top_stringt   exe_strt   imageStringt   f(    (    sS   /home/kylunr01/Documents/Nitro/Autoreporting/Vacancy_RAG/htmlStringConstants_RAG.pyt    locationPerformance_includeImage�  s    (   t   datetimet   base64R6   t   matht   headert   firstRedt
   firstAmbert   rollupLocationt   rollupHiringManagert   missingHiringManagert   startDateInPastt   noPFIDWindowt   fullRedAmbert   amberSectionHeadert   footert   midTableBreakR   R&   R)   R*   R4   RA   (    (    (    sS   /home/kylunr01/Documents/Nitro/Autoreporting/Vacancy_RAG/htmlStringConstants_RAG.pyt   <module>   s*   �	*,!/,/0	&				